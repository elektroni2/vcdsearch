"""
# TODO: update documentation

Functions to transform a VCD object's time value pairs to binary sequences.

The clock period is estimated to be the smallest time difference between all
time differences between the time instants of the signal. The rationale for
this is that the driver component is known to update the signal value at most
every smallest time difference.

The estimation fails if the true smallest time difference between value changes
is not present in the current time value pairs dataset.

Example of a estimation failure:
--------------------------------

The driver component attempts to send the following binary sequence:
    000111000111
The time value pairs produced are following:
    t0      : 0
    t0 +  dt: 1
    t0 + 2dt: 0
    t0 + 3dt: 1
The binarizer calculates that the minimum time difference is dt, so it must be
the clock period of the signal thus producing the following binary sequence:
    0101
The correct clock period would be dt / 3.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import logging

from math import modf, sqrt
from typing import Dict, Iterable, Set, List, Counter, Optional, TextIO, Tuple, Union
from pathlib import Path
from functools import reduce, lru_cache
from itertools import islice

# =============================================================================
# Local module imports
# =============================================================================

from vcdbus import Bus, BusName
from vcdobject import VCDObject
from vcdsignal import Signal, SignalName
from vcdtiming import Timing
from vcdutilities import get_progress_bar_wrapper
from vcdtimescale import TimeScale
from vcdtimescaleunit import TimeScaleUnit

# =============================================================================
# Function definitions
# =============================================================================


def combine_separated_bus_wires(
        vcd: VCDObject,
        show_progress_bars: bool) -> None:
    """
    Identify signal groups and transform them to buses.

    Parameters
    ----------
    vcd: VCDObject
        The VCD object which contains the signals to be combined to buses.
    show_progress_bars: bool
        Print the progress bars to the console.
    """

    if not isinstance(vcd, VCDObject):
        raise Exception(f"VCD must be a VCDObject.")
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"Show progress bars option must be a boolean.")

    from typing import ContextManager

    # ! get rid of the show progress bars variable with conditional definition?
    # ! if enabled: define f, else define empty wrapper
    # The progress bar wrapper.
    def progress_bar(iterable: Iterable, description: str) -> ContextManager:
        from vcdutilities import get_progress_bar_wrapper
        pbw = get_progress_bar_wrapper(show_progress_bars)
        return pbw(iterable, desc=description)

    # Group signals that belong to same buses.
    # would deque be faster? is uniqueness necessary?
    groups: Dict[str, Set[Signal]] = dict()
    for signal in progress_bar(vcd.signals, "Signals grouped"):
        if signal.name.bus_description:
            path_and_name = signal.name.with_path(without_bus_description=True)
            if path_and_name in groups.keys():
                groups[path_and_name].add(signal)
            else:
                groups[path_and_name] = {signal}

    if not groups:
        return

    del signal
    del path_and_name

    # To avoid re-evaluating the dot expression.
    vcd_signals = vcd.signals
    vcd_buses = vcd.buses

    # Transform the signal groups to buses and add them to the VCD object bus
    # collection.
    for group_name, signals in progress_bar(
            groups.items(),
            "Groups converted to buses"):
        size = len(signals)

        bus_signals: Dict[int, Signal] = {}
        while len(signals) != 0:
            signal = signals.pop()

            # All bus signals are the same kind.
            kind = signal.kind

            # Add the signal to correct position in the bus.
            bus_signals[signal.name.bus_description_as_int()] = signal

            # Because the signal is now part of a bus it can be removed
            # from the VCD object signals list.
            vcd_signals.remove(signal)

        index_range = (max(bus_signals.keys()), min(bus_signals.keys()))

        # rd = range description
        without_rd = group_name
        rd = f"[{index_range[0]}:{index_range[1]}]"

        path = group_name.rsplit(".", 1)[0]

        name = BusName(path, without_rd, rd)

        bus = Bus(name, kind, size, index_range)
        bus.signals = bus_signals
        vcd_buses.add(bus)


# ! HOT CODE INSIDE
def separate_combined_bus_wires(
        vcd: VCDObject,
        show_progress_bars: bool) -> None:
    """
    Transform buses to groups of signals.

    Parameters
    ----------
    vcd: VCDObject
        The VCD object which contains the buses to be separated to signals.
    show_progress_bars: bool
        Print the progress bars to the console.
    """

    if not isinstance(vcd, VCDObject):
        raise Exception(f"VCD must be a VCDObject.")
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"Show progress bars option must be a boolean.")

    from typing import ContextManager

    # The progress bar wrapper.
    def progress_bar(iterable: Iterable, description: str) -> ContextManager:
        from vcdutilities import get_progress_bar_wrapper
        pbw = get_progress_bar_wrapper(show_progress_bars)
        return pbw(iterable, desc=description)

    for bus in progress_bar(vcd.buses, "Buses separated"):
        if len(bus.signals) != 0:
            # Bus has its signals already separated.
            continue

        # Separated signals where the key is the signal index in the bus.
        separated: Dict[int, Signal] = dict()

        signal_range = bus.range
        if signal_range:
            # The bus has a asymmetric range. For example bus[3:1]. It is
            # not possible to start the iteration from the zeroth index.
            msb_index, lsb_index = signal_range
            index_range = range(lsb_index, msb_index + 1)

        else:
            # The bus has a symmetric range. For example bus[3:0].
            # It is possible to start the iteration from zeroth index.
            index_range = range(0, bus.size)

        # ! Very slow n^m loop...
        without_bd = bus.name.without_range_description
        bus_tv_items = bus.tv.items()
        bus_kind = bus.kind
        for index in index_range:
            # Create a name for the separated signal.
            bd = str(index)
            with_bd = "".join([without_bd, "[", bd, "]"])
            name = SignalName(with_bd, without_bd, bd)

            signal = Signal(name, bus_kind)
            separated[index] = signal

            # ! HOT CODE
            # Separate the signal's time value pairs from the bus time value
            # pairs. Explanation: ti = time_instant, cv = combined_value.
            signal.tv = {
                ti: cv[index] for ti, cv in bus_tv_items
            }

        bus.signals = separated

# -----------------------------------------------------------------------------
# Functions to solve signal binary sequences.
# -----------------------------------------------------------------------------


def solve_signal_time_differences(
        signal: Signal) -> Optional[Counter[int]]:
    """
    Calculate signal's all time differences between it's time instants.

    Parameters
    ----------
    signal: Signal
        The target signal.

    Returns
    -------
    Counter[int]
        Target signal's all time differences as a counter where the counter
        keys are the time differences between the time instants and the counter
        values are the frequencies of the time differences.
    None
        Could not solve time differences because there were too few time value
        pairs.
    """

    time_differences: Counter[int] = Counter()

    instants = list(signal.tv.keys())

    if 0 in instants:
        instants.remove(0)

    if len(instants) < 2:
        return None

    for index in range(1, len(instants)):
        instant1 = instants[index - 1]
        instant2 = instants[index]
        td = instant2 - instant1
        time_differences[td] += 1

    return time_differences


# ! HOT CODE, CACHED
# ! very hard to understand what is going on
@lru_cache()
def get_divisors(number: int) -> Set[int]:
    """The optimized approach."""
    step = 2 if number % 2 else 1
    return set(
        reduce(
            list.__add__,
            ([i, number//i] for i in range(1, int(sqrt(number))+1, step) if number % i == 0)
        )
    )


def get_divisors2(number: int) -> Set[int]:
    """The naive approach."""
    divisors: Set[int] = set()
    divisors_add = divisors.add
    for i in range(1, number + 1):
        if number % i == 0:
            divisors_add(i)
    return divisors


# ! HOT CODE
# ? deprecated?
def solve_signal_clock_period(signal: Signal) -> Optional[int]:
    instants = list(signal.tv.keys())
    if 0 in instants:
        instants.remove(0)
    if len(instants) < 2:
        signal.binarization_failed = True
        return None
    divisor_groups = []
    for index in range(1, len(instants)):
        instant1 = instants[index - 1]
        instant2 = instants[index]
        time_difference = instant2 - instant1
        divisors = get_divisors(time_difference)
        divisor_groups.append(divisors)
    common_divisors = set(divisor_groups[0].copy())
    remove_set = set()
    for group in divisor_groups[1:]:
        for common_divisor in common_divisors:
            if common_divisor not in group:
                remove_set.add(common_divisor)
    for removable in remove_set:
        common_divisors.remove(removable)
    clock_period = max(common_divisors)
    return clock_period


# ! UNSTABLE
def solve_signal_clock_period_opt(
        signal: Signal,
        time_differences: List[int]) -> Optional[int]:
    if 0 in signal.tv.keys():
        td_count = len(time_differences) - 1
        td_iterator = islice(time_differences, 1, len(time_differences))
    else:
        td_count = len(time_differences)
        td_iterator = islice(time_differences, 0, len(time_differences))
    #removed_difference = None
    #if 0 in signal.tv.keys():
    #    removed_difference = time_differences[0]
    #    time_differences = time_differences[1:]
    if td_count < 2:
        signal.binarization_failed = True
        return None
    #instants = list(signal.tv.keys())
    #if 0 in instants:
    #    instants.remove(0)
    #if len(instants) < 2:
    #    signal.binarization_failed = True
    #    return None
    divisor_groups = [get_divisors(ti) for ti in td_iterator]

    # ! If time differences are equal, then the common divisors are the same

    #divisor_groups = []
    #for index in range(1, len(instants)):
    #    instant1 = instants[index - 1]
    #    instant2 = instants[index]
    #    time_difference = instant2 - instant1
    #    divisors = get_divisors(time_difference)
        #for i in range(1, time_difference + 1):
        #    if time_difference % i == 0:
        #        divisors.append(i)
    #    divisor_groups.append(divisors)
    index_smallest_group = 0
    for index in range(len(divisor_groups)):
        current_size = len(divisor_groups[index_smallest_group])
        candidate_size = len(divisor_groups[index])
        if candidate_size < current_size:
            index_smallest_group = index

    common_divisors = set(divisor_groups[index_smallest_group].copy())

    #common_divisors = set(divisor_groups[0].copy())
    remove_set = set()
    for group in divisor_groups:
    #for group in divisor_groups[1:]:
        for common_divisor in common_divisors:
            if common_divisor not in group:
                remove_set.add(common_divisor)
    for removable in remove_set:
        common_divisors.remove(removable)
    clock_period = max(common_divisors)
    #if removed_difference:
    #    time_differences.insert(0, removed_difference)
    return clock_period


# ? deprecated?
def solve_signal_binary_length(
        signal: Signal,
        clock_period: int) -> Optional[Tuple[int, int]]:
    binary_length = 0
    instants = list(signal.tv.keys())
    binary_start_instant = instants[0]
    for index in range(1, len(instants)):
        instant1 = instants[index - 1]
        instant2 = instants[index]
        time_difference = instant2 - instant1
        subsequence_length = time_difference / clock_period
        if instant1 == 0:
            # Special case. The signal value might be initialized at zero, but
            # its binary sequence is not aligned by the zero.
            frac, _whole = modf(subsequence_length)
            if frac != 0:
                # Signal is not aligned by zero.
                binary_start_instant = instant2
                continue
            # We can safely add the bits starting from zero instant.
        binary_length += round(subsequence_length, None)
    binary_length += 1
    return (binary_length, binary_start_instant)


# ! UNSTABLE
def solve_signal_binary_length_opt(
        signal: Signal,
        clock_period: int,
        time_differences: List[int]) -> Optional[Tuple[int, int]]:
    binary_length = 0
    instants = list(signal.tv.keys())
    binary_start_instant = instants[0]
    instant1, instant2 = list(signal.tv.keys())[0:2]
    first_is_zero = instant1 == 0
    for time_difference in time_differences:
        subsequence_length = time_difference / clock_period
        if first_is_zero:
            first_is_zero = False
            # Special case. The signal value might be initialized at zero, but
            # its binary sequence is not aligned by the zero.
            frac, _whole = modf(subsequence_length)
            if frac != 0:
                # Signal is not aligned by zero.
                binary_start_instant = instant2
                continue
            # We can safely add the bits starting from zero instant.
        binary_length += round(subsequence_length, None)
    binary_length += 1
    return (binary_length, binary_start_instant)


# ! binary[binary_index] list assignment out of range
def solve_signal_binary_sequence(
        signal: Signal,
        clock_period: int,
        binary_length: int,
        binary_start_instant: int) -> str:
    binary = ["-"] * binary_length
    pairs = list(signal.tv.items())
    if len(pairs) == 1:
        binary = [pairs[0][1]] * binary_length
        return "".join(binary)
    binary_index = 0
    for index in range(1, len(pairs)):
        instant1, value1 = pairs[index - 1]
        # Skip until the binary start instant.
        if instant1 < binary_start_instant:
            continue
        instant2, value2 = pairs[index]
        time_difference = instant2 - instant1
        subsequence_length = round(time_difference / clock_period, None)
        for _sub_index in range(subsequence_length):
            binary[binary_index] = value1
            binary_index += 1
    while binary_index < binary_length:
        binary[binary_index] = value2
        binary_index += 1
    return "".join(binary)


def solve_signal_binary(
        vcd: VCDObject,
        signal: Signal,
        clock_periods: Dict[str, Union[TimeScale, str]]) -> None:
    # -----------------------
    # Solve time differences.
    # -----------------------
    instants = list(signal.tv.keys())
    if len(instants) == 1:
        # We know that the binary sequence is a constant, but no timing
        # information can be acquired.
        signal.binary = signal.tv[instants[0]]
        signal.timing = None
        return
    time_differences = [instants[i] - instants[i - 1] for i in range(1, len(instants))]
    if not time_differences:
        signal.binarization_failed = True
        return
    # -------------------
    # Solve clock period.
    # -------------------
    if signal.name.with_bus_description() in clock_periods.keys():
        # We have background knowledge about the clock period. We assume that
        # the given clock period is the true clock period so we do not perform
        # any checks.
        clock_period = clock_periods[signal.name.with_bus_description()]
        if isinstance(clock_period, TimeScale):
            clock_period_known = True
            clock_period_int = clock_period.number
        else:
            clock_period_known = False
    else:
        clock_period_int = solve_signal_clock_period_opt(
            signal,
            time_differences
        )
        if clock_period_int:
            clock_period_known = True
            clock_period = TimeScale(
                clock_period_int,
                vcd.simulation_time_scale.unit
            )
        else:
            clock_period_known = False
    if not clock_period_known:
        signal.binarization_failed = True
        return
    # --------------------
    # Solve binary length.
    # --------------------
    result = solve_signal_binary_length_opt(
        signal,
        clock_period_int,
        time_differences
    )
    if not result:
        signal.binarization_failed = True
        return
    binary_length, binary_start_instant = result
    args = (signal, clock_period_int, binary_length, binary_start_instant)
    signal.binary = solve_signal_binary_sequence(*args)
    signal.timing = Timing(binary_start_instant, clock_period)


def binary_search(array: List[int], target: int) -> int:
    """
    TODO
    """

    low = 0
    high = len(array) - 1
    middle = 0

    while low <= high:
        middle = (high + low) // 2
        if array[middle] < target:
            # Ignore left half.
            low = middle + 1
        elif target < array[middle]:
            # Ignore right half.
            high = middle - 1
        else:
            # Target found at index stored in middle.
            return middle

    # Target was not present.
    return -1


def solve_bus_binary(
        vcd: VCDObject,
        bus: Bus,
        clock_periods: Dict[str, Union[TimeScale, str]]) -> None:
    bus_signals_values = bus.signals.values()
    # -----------------------
    # Solve the clock period.
    # -----------------------
    if bus.name.with_range_description() in clock_periods.keys():
        # We have background knowledge about the bus clock period. We assume
        # that the given clock period is the true clock period, no checks are
        # performed.
        clock_period = clock_periods[bus.name.with_range_description()]
        if isinstance(clock_period, TimeScale):
            clock_period_known = True
            clock_period_int = clock_period.number
        else:
            clock_period_known = False
    else:
        # We assume that the bus is homogenous in terms of its signals' clock
        # periods so all signals obey the same clock.
        all_clock_periods = set()
        for signal in bus_signals_values:
            # Solve time differences.
            instants = list(signal.tv.keys())
            time_differences = [instants[i] - instants[i - 1] for i in range(1, len(instants))]
            if not time_differences:
                signal.binarization_failed = True
                continue
            clock_period = solve_signal_clock_period_opt(
                signal,
                time_differences
            )
            if clock_period:
                all_clock_periods.add(clock_period)
        del instants
        del time_differences
        if not all_clock_periods:
            bus.binarization_failed = True
            for signal in bus_signals_values:
                signal.binarization_failed = True
            return
        clock_period_known = True
        clock_period_int = min(all_clock_periods)
        clock_period = TimeScale(
            clock_period_int,
            vcd.simulation_time_scale.unit
        )
        del all_clock_periods
    if not clock_period_known:
        bus.binarization_failed = True
        for signal in bus_signals_values:
            signal.binarization_failed = True
        return
    del clock_period_known
    # ------------------------
    # Solve the binary length.
    # ------------------------
    # We assume that all signals have the same binary length.
    all_binary_lengths = set()
    all_binary_start_instants = set()
    for signal in bus_signals_values:
        result = solve_signal_binary_length(signal, clock_period_int)
        if result:
            binary_length, binary_start_instant = result
            all_binary_lengths.add(binary_length)
            all_binary_start_instants.add(binary_start_instant)
    del signal
    # Signals can have different binary lengths the because binary strings are
    # compressed, so we take the longest binary length because it is suitable
    # for all the signals.
    binary_length = max(all_binary_lengths)
    # Signals can have different start instants so we take the earliest start
    # because it is suitable for all the signals.
    binary_start_instant = min(all_binary_start_instants)
    del all_binary_lengths
    del all_binary_start_instants
    # ------------------------------
    # Binarize the separate signals.
    # ------------------------------
    bus.timing = Timing(binary_start_instant, clock_period)
    for signal in bus_signals_values:
        args = (signal, clock_period_int, binary_length, binary_start_instant)
        signal.binary = solve_signal_binary_sequence(*args)
        signal.timing = bus.timing
        signal.binarization_failed = False  # ! is this safe?
    del args
    del signal
    del binary_length
    # -------------------------------
    # Solve the bus time value pairs.
    # -------------------------------
    if 0 < len(bus.tv):
        # The bus was already combined, so it has the time value pairs already.
        return
    # The bus was built from separated signals, so the time value pairs must be
    # computed from the signals.
    instants_set: Set[int] = set()
    instants_set_add = instants_set.add
    # Get instants that are larger than the binary start instant.
    for signal in bus_signals_values:
        # We assume that the instants are in order so we can binary search the
        # binary start index and ignore the earlier instants.
        instants = list(signal.tv.keys())
        start_index = binary_search(instants, binary_start_instant)
        for instant_index in range(start_index, len(instants)):
            instants_set_add(instants[instant_index])
    instants = sorted(list(instants_set))
    # If only a subset of bus's signals are targeted, then modifying the value
    # by index can assign index out of range.
    combined_value_width = max(bus.signals.keys()) + 1
    # To avoid rebuilding the list for each iteration.
    combined_value_template = ["-"] * combined_value_width
    # To avoid re-evaluating the dot-expression.
    bus_signals_items = bus.signals.items()

    # ! remove dependency to tqdm
    from tqdm import tqdm
    for instant in tqdm(instants):
        combined_value = combined_value_template[:]
        for signal_index, signal in bus_signals_items:
            if instant in signal.tv.keys():
                signal_value = signal.tv[instant]
            else:
                # Solve the earlier instant (ei).
                ei = max(ei for ei in signal.tv.keys() if ei < instant)
                signal_value = signal.tv[ei]
            combined_value[signal_index] = signal_value
        bus.tv[instant] = "".join(combined_value)

# -----------------------------------------------------------------------------
# The binarization function.
# -----------------------------------------------------------------------------


# ! what if vcd object time scale is None?
def execute_binarization(
        vcd: VCDObject,
        clock_periods: Dict[str, Union[TimeScale, str]],
        show_progress_bars: bool) -> None:
    """
    Transform a signal's or bus's time value pair representation to a binary
    sequence representation.

    Binarization is performed by first estimating the clock period that the
    signal or bus obeys. This gives the time period between two symbols in the
    binary sequence.

    Binarization is then performed by calculating the amount of bits between
    two time value pair instants. After that the previous value is just copied
    until a new value appears.

    Parameters
    ----------
    vcd: VCDObject
        The VCD object which contains the target signals and buses to be
        binarized.
    clock_periods: Dict[str, Union[TimeScale, str]]
        Background knowledge about the objects' clock periods.
    show_progress_bars: bool
        Print the progress bars to the console.
    """

    if not isinstance(vcd, VCDObject):
        raise Exception(f"The VCD object must be a VCD object.")
    if not isinstance(clock_periods, dict):
        raise Exception(f"The clock periods must be a dictionary.")
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"The show progress bars options must be a boolean.")

    # ----------------------------------
    # The binarization preparation phase
    # ----------------------------------

    combine_separated_bus_wires(vcd, show_progress_bars)
    separate_combined_bus_wires(vcd, show_progress_bars)

    # ----------------------
    # The binarization phase
    # ----------------------

    progress_bar = get_progress_bar_wrapper(show_progress_bars)

    # Build the binaries for the signals which are not part of buses.
    for signal in progress_bar(vcd.signals, desc="Signals binarized"):
        solve_signal_binary(vcd, signal, clock_periods)

    # Build the binaries for the buses.
    for bus in progress_bar(vcd.buses, desc="Buses binarized"):
        solve_bus_binary(vcd, bus, clock_periods)

# -----------------------------------------------------------------------------
# File functions.
# -----------------------------------------------------------------------------


def read_clock_periods(stream: TextIO) -> Dict[str, TimeScale]:
    """
    Read object names and clock periods from a clock periods stream.

    Parameters
    ----------
    stream: TextIO
        The text stream of the file which contains the object names and their
        clock periods.

    Returns
    -------
    Dict[str, Union[TimeScale, str]]
        The clock periods by the object names. Clock period can also be
        unknown.
    """

    from re import search

    from vcdstringtools import \
        process_bracket_expression as pbe, \
        pattern_identifier_and_time_scale as piats

    result: Dict[str, TimeScale] = {}

    line_number = 1
    while True:
        line = stream.readline()

        if not line:
            # End of file.
            break

        # Remove surrounding white space.
        line = line.strip()

        # Exclude empty lines.
        if line == "":
            continue

        # Exclude lines that begin with a number sign.
        if line[0] == "#":
            continue

        results = search(piats, line)
        if not results:
            raise Exception(f"Could not parse line {line_number}.")

        # ---------------------
        # Get the clock period.
        # ---------------------

        clock_period: Optional[Union[str, TimeScale]] = None
        if results.group("time_scale_expr") == "?":
            clock_period = "unknown"
        else:
            number = int(results.group("number"))
            unit = results.group("unit")
            clock_period = TimeScale(number, TimeScaleUnit.from_string(unit))

        # --------------------
        # Get the object name.
        # --------------------

        path = results.group("path")
        identifier = results.group("identifier")
        bracket = results.group("bracket")

        if not identifier:
            raise Exception(f"Could not find the object name at line "
                            "{line_number}.")

        name = identifier
        if path:
            # Add the path to the identifier name.
            name = ".".join([path, name])

        if bracket:
            bracket_info = pbe(bracket)
            if bracket_info.is_single_signal:
                index = bracket_info.signal_index
                full_name = "".join([name, "[", str(index), "]"])
                result[full_name] = clock_period
            elif bracket_info.is_bus:
                bus_name = "".join([name, bracket])
                result[bus_name] = clock_period
                for index in bracket_info.signal_index_range:
                    full_name = "".join([name, "[", str(index), "]"])
                    result[full_name] = clock_period
            elif bracket_info.is_double_colon_expression:
                for index in bracket_info.signal_index_range:
                    full_name = "".join([name, "[", str(index), "]"])
                    result[full_name] = clock_period
        else:
            result[name] = clock_period

        line_number += 1

    return result

# -----------------------------------------------------------------------------
# Reporting functions.
# -----------------------------------------------------------------------------


def write_binarization_report_to_json_file(
        path: Path,
        vcd: VCDObject):
    import json

    data: Dict[str, Dict] = dict()

    data["signals"] = dict()
    for signal in vcd.signals:
        name = signal.name.with_bus_description()
        data["signals"][name] = signal.to_dictionary()

    data["buses"] = dict()
    for bus in vcd.buses:
        name = bus.name.with_range_description()
        data["buses"][name] = bus.to_dictionary()

    with path.open("w") as file_handle:
        json.dump(data, file_handle)


def write_binarization_report_to_xml_file(
        path: Path,
        vcd: VCDObject):
    from xml.etree.ElementTree import Element, SubElement, tostring

    root = Element("binarization_report")

    signals_item = SubElement(root, "signals")
    for signal in vcd.signals:
        signal_data = signal.to_dictionary()
        signal_binary = signal_data.pop("binary", None)
        signal_item = SubElement(signals_item, "signal", attrib=signal_data)
        signal_item.text = signal_binary

    buses_item = SubElement(root, "buses")
    for bus in vcd.buses:
        bus_data = bus.to_dictionary()
        bus_signals = bus_data.pop("signals")
        bus_item = SubElement(buses_item, "bus", attrib=bus_data)
        for signal_data in bus_signals.values():
            signal_binary = signal_data.pop("binary", None)
            signal_item = SubElement(bus_item, "signal", attrib=signal_data)
            if signal_binary:
                signal_item.text = signal_binary

    data = tostring(root)
    with path.open("wb") as file_handle:
        file_handle.write(data)

# =============================================================================
# Script execution interface.
# =============================================================================


if __name__ == "__main__":
    from vcdutilities import check_user_python_version
    check_user_python_version()

    import pickle
    import argparse

    from vcdparser import ParserState

    cmdparser = argparse.ArgumentParser(
        prog=f"vcdbinarizer",
        usage=None,
        description=f"TODO",
        epilog=f"TODO",
        parents=[],
        formatter_class=argparse.HelpFormatter,
        prefix_chars="-",
        fromfile_prefix_chars=None,
        argument_default=None,
        conflict_handler="error",
        add_help=True,
        allow_abbrev=True,
        exit_on_error=True
    )

    # --------------------
    # Mandatory arguments.
    # --------------------

    cmdparser.add_argument(
        "input",
        type=Path,
        help="Full path to the VCD object file."
    )
    cmdparser.add_argument(
        "output",
        type=Path,
        help="Full path to the file where the binarized VCD object is "
             "serialized to."
    )

    # ---------------------------------
    # Binarization fine tuning options.
    # ---------------------------------

    cmdparser.add_argument(
        "-cpf", "--clock-periods-file",
        type=Path,
        help="Full path to a file that contains clock periods for all or some "
             "of the signals and buses."
    )

    # ------------------
    # Reporting options.
    # ------------------

    cmdparser.add_argument(
        "-wr", "--write-report",
        nargs="*",
        type=Path,
        help="List of full paths to files where the binarization reports are "
             "written to separated by spaces."
    )

    # -----------------------
    # User interface options.
    # -----------------------

    cmdparser.add_argument(
        "-wptc", "--write-progress-to-console",
        action="store_true",
        help="Let the script to report it's progress to the console."
    )
    cmdparser.add_argument(
        "-spb", "--show-progress-bars",
        action="store_true",
        help="Let the script to show it's parsing progress with progress bar."
    )

    # --------------
    # Other options.
    # --------------

    cmdparser.add_argument(
        "-pp", "--profile-performance",
        type=Path,
        help="Full path to the file where a performance report is written to."
    )

    args = vars(cmdparser.parse_args())

    # ------------------------------
    # Mandatory arguments unpacking.
    # ------------------------------

    input_file = args.get("input", None)
    if not isinstance(input_file, Path):
        raise Exception(f"The input file must be a file.")
    if not input_file.exists():
        raise Exception(f"The input file must exist.")
    if not input_file.is_file():
        raise Exception(f"The input file must be a file.")
    if input_file.suffix != ".pickle":
        raise Exception(f"The input file extension must be .pickle.")

    output_file = args.get("output", None)
    if not isinstance(output_file, Path):
        raise Exception(f"The output must be a file.")
    if not output_file.exists():
        raise Exception(f"The output file must exist.")
    if not output_file.is_file():
        raise Exception(f"The output file must be a file.")
    if output_file.suffix != ".pickle":
        raise Exception(f"The output file extension must be .pickle.")

    # -------------------------------------------
    # Binarization fine tuning options unpacking.
    # -------------------------------------------

    clock_periods: Dict[str, TimeScale] = dict()
    clock_periods_file = args.get("clock_periods_file", None)
    if clock_periods_file:
        if not isinstance(clock_periods_file, Path):
            raise Exception(f"The clock periods file must be a file.")
        if not clock_periods_file.exists():
            raise Exception(f"The clock periods file must exist.")
        if not clock_periods_file.is_file():
            raise Exception(f"The clock periods file must be a file.")
        if clock_periods_file.suffix != ".txt":
            raise Exception(f"The clock periods file extension must be .txt.")

        with clock_periods_file.open("r") as stream:
            clock_periods = read_clock_periods(stream)

    # ----------------------------
    # Reporting options unpacking.
    # ----------------------------

    report_files = args.get("write_report", None)
    if report_files:
        for path in report_files:
            if not isinstance(path, Path):
                raise Exception(f"The report file must be a file.")
            if not path.exists():
                raise Exception(f"The report file must exist.")
            if not path.is_file():
                raise Exception(f"The report file must be a file.")
            if path.suffix not in {".json", ".xml"}:
                raise Exception(f"The report file extension must be .json or "
                                ".xml.")
    else:
        report_files = []

    # ---------------------------------
    # User interface options unpacking.
    # ---------------------------------

    write_progress_to_console = args.get("write_progress_to_console", False)
    if not isinstance(write_progress_to_console, bool):
        raise Exception(f"The write progress to console option must be a "
                        "boolean.")

    if write_progress_to_console:
        # The user wants to read scipt progress reports.
        logging.basicConfig(level=logging.INFO)
    else:
        # Report only if something goes wrong.
        logging.basicConfig(level=logging.WARNING)

    show_progress_bars = args.get("show_progress_bars", False)
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"The show progress bars option must be a boolean.")

    # --------------
    # Other options.
    # --------------

    profile_performance = args.get("profile_performance", None)
    if profile_performance:
        if not isinstance(profile_performance, Path):
            raise Exception(f"The profile performance must be a file.")
        if not profile_performance.exists():
            raise Exception(f"The profile performance file must exist.")
        if not profile_performance.is_file():
            raise Exception(f"The profile performance file must be a file.")
        if profile_performance.suffix != ".txt":
            raise Exception(f"The profile performance file extension must be "
                            ".txt.")

    # -----------------------
    # The binarization phase.
    # -----------------------

    logging.info(f"Deserializing the VCD object file.")
    with input_file.open("rb") as input_stream:
        parser_state: ParserState = pickle.load(input_stream)
    logging.info(f"Deserialization finished.")

    logging.info(f"Binarizing the VCD object.")
    binarization_args = (
        parser_state.vcd_object,
        clock_periods,
        show_progress_bars
    )
    if profile_performance:
        # Binarize and profile.
        from cProfile import Profile
        from pstats import Stats, SortKey

        with Profile() as pr:
            try:
                execute_binarization(*binarization_args)
                logging.info("Binarization finished.")
            except Exception:
                # ! handle me
                logging.info("Binarization failed.")
                pass

        logging.info(f"Writing the performance report.")
        with profile_performance.open("w") as stream:
            stats = Stats(pr, stream=stream)
            stats.strip_dirs()
            stats.sort_stats(SortKey.TIME)
            stats.print_stats()
        logging.info(f"Performance report finished.")
    else:
        # Just binarize.
        execute_binarization(*binarization_args)
        logging.info(f"Binarization finished.")

    logging.info(f"Serializing and saving the binarized VCD object.")
    with output_file.open("wb") as output_stream:
        pickle.dump(parser_state, output_stream)
    logging.info(f"Serialization finished.")

    if report_files:
        logging.info(f"Writing the binarization report(s).")
    for path in report_files:
        reporting_args = (path, parser_state.vcd_object)
        if path.suffix == ".json":
            write_binarization_report_to_json_file(*reporting_args)
        elif path.suffix == ".xml":
            write_binarization_report_to_xml_file(*reporting_args)

    logging.info(f"Program finished.")
