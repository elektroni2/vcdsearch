"""
Functions to search target binary sequences from a binarized VCD object.

# ! search reports contain findings for ex. instant 120 when clock period is
# ! 100 ns. should this be expressed as 1.2 with offset 0.2 and cp 100 ns?
# ! this way you only need to multiply 1.2 and 0.2 with 100 ns to get the
# ! real value. but how about floating point accuracy? is it safe?

"""

# =============================================================================
# Standard library imports
# =============================================================================

import pathlib
import logging

from typing import Any, Iterable, Set, List, Dict, Union, Tuple

# =============================================================================
# Local module imports
# =============================================================================

from vcdbus import Bus
from vcdsignal import Signal
from vcdobject import VCDObject
from vcdutilities import SCALAR_DIGITS

# =============================================================================
# SearchFindings definition
# =============================================================================


# dict[sequence : (object name, [time instants])]


class SearchFindings:
    """
    A container for search findings from a signal or a bus.

    Which signal or bus and at which times?

    Attributes
    ----------
    TODO
    """

    def __init__(self, target_object: Union[Signal, Bus]) -> None:
        self.target_object: Union[Signal, Bus] = target_object
        self.time_instants_signals: List[int] = list()
        self.time_instants_buses: Dict[int, List[List[int]]] = dict()
        self.time_instant_ranges: List[Tuple[int, int]] = list()

    def __repr__(self) -> str:
        return f"Findings from {self.target_object}"

# =============================================================================
# SearchResults definition
# =============================================================================


class SearchResults:
    """
    A container for the binary sequence search results.

    Which sequence is found at which signal or bus and at which times?

    Attributes
    ----------
    time_instant_findings : Dict[str, Any]
        Search findings from time instants. The dictionary key is the object
        type group for example signals or buses and the dictionary value is the
        finding object.

    time_range_findings : Dict[str, Any]
        Search findings from time ranges. The dictionary key is the sequence
        object type group for example signals or buses and the dictionary value
        is the finding object.
    """

    def __init__(self) -> None:
        self.time_instant_findings: Dict[str, Any] = {}
        self.time_range_findings: Dict[str, Any] = {}

# =============================================================================
# Function definitions
# =============================================================================


def find_sequences_from_time_instants(
        result: SearchResults,
        signals: Set[Signal],
        buses: Set[Bus],
        sequences: List[str],
        show_progress_bars: bool) -> None:
    """
    TODO

    Parameters
    ----------
    result: SearchResults
        TODO
    signals: Set[Signal]
        TODO
    buses: Set[Bus]
        TODO
    sequences: List[str]
        TODO
    show_progress_bars: bool
        TODO
    """

    if not isinstance(result, SearchResults):
        raise Exception(f"Result must be a SearchResults object.")
    if not isinstance(signals, set):
        raise Exception(f"Signals must be a set.")
    if not isinstance(buses, set):
        raise Exception(f"Buses must be a set.")
    if not isinstance(sequences, list):
        raise Exception(f"Sequences must be a list.")
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"Show progress bars option must be a boolean.")

    from typing import ContextManager

    # The progress bar wrapper.
    def progress_bar(iterable: Iterable) -> ContextManager:
        from vcdutilities import get_progress_bar_wrapper
        pbw = get_progress_bar_wrapper(show_progress_bars)
        return pbw(iterable, desc="Sequences searched from time instants")

    from re import compile

    findings_by_sequence: Dict[str, Any] = {}
    for sequence in progress_bar(sequences):
        pattern = compile(r"({sequence})".format(sequence=sequence))

        findings_by_sequence[sequence] = {}

        # Signal has only 1 value per time instant.
        if len(sequence) == 1:
            findings_by_signal: Dict[str, List[int]] = {}
            for signal in signals:
                findings_in_signal = {"findings": []}
                if signal.timing:
                    findings_in_signal["offset"] = f"{signal.timing.offset} {signal.timing.clock_period.unit}"
                    findings_in_signal["clock_period"] = f"{signal.timing.clock_period.number} {signal.timing.clock_period.unit}"
                else:
                    findings_in_signal["offset"] = "0"
                    findings_in_signal["clock_period"] = "unknown"
                for time_instant, value in signal.tv.items():
                    if sequence in value:
                        findings_in_signal["findings"].append(time_instant)
                findings_by_signal[signal.name.with_bus_description()] = findings_in_signal
            findings_by_sequence[sequence]["signals"] = findings_by_signal

        findings_by_bus: Dict[str, Any] = dict()
        for bus in filter(lambda b: len(sequence) <= b.size, buses):
            findings_in_bus = {"findings": list()}
            if bus.timing:
                findings_in_bus["offset"] = f"{bus.timing.offset} {bus.timing.clock_period.unit}"
                findings_in_bus["clock_period"] = f"{bus.timing.clock_period.number} {bus.timing.clock_period.unit}"
            else:
                findings_in_bus["offset"] = "0"
                findings_in_bus["clock_period"] = "unknown"
            if len(sequence) == 1:
                # Sequence is found at single signals.
                findings_by_instant: Dict[int, List[int]] = dict()
                for time_instant, value in bus.tv.items():
                    instant_findings: List[int] = list()
                    matches = pattern.finditer(value)
                    for match in matches:
                        index = match.start()
                        # Add signal index to findings.
                        instant_findings.append(index)
                    if instant_findings:
                        findings_by_instant[time_instant] = instant_findings
                findings_in_bus["findings"].append(findings_by_instant)
            else:
                # Sequence is found at multiple signals.
                findings_by_instant: Dict[int, List[List[int]]] = dict()
                for time_instant, value in bus.tv.items():
                    if sequence in value:
                        # Search from all substrings.
                        findings_in_instant: List[List[int]] = list()
                        # To avoid re-evaluating the dot-expression.
                        pattern_match = pattern.match
                        fii_append = findings_in_instant.append
                        for start_index in range(len(value)):
                            match = pattern_match(value, start_index)
                            if match:
                                index0, index1 = match.span()
                                signal_range = list(range(index0, index1))
                                fii_append(signal_range)
                        findings_by_instant[time_instant] = findings_in_instant
                findings_in_bus["findings"].append(findings_by_instant)
            findings_by_bus[bus.name.with_range_description()] = findings_in_bus
        findings_by_sequence[sequence]["buses"] = findings_by_bus

    result.time_instant_findings = findings_by_sequence


def find_sequences_from_time_ranges(
        result: SearchResults,
        signals: Set[Signal],
        buses: Set[Bus],
        sequences: List[str],
        show_progress_bars: bool) -> None:
    """
    TODO

    Parameters
    ----------
    result: SearchResults
        TODO
    signals: Set[Signal]
        TODO
    buses: Set[Bus]
        TODO
    sequences: List[str]
        TODO
    show_progress_bars: bool
        TODO
    """

    if not isinstance(result, SearchResults):
        raise Exception(f"Result must be a SearchResults object.")
    if not isinstance(signals, set):
        raise Exception(f"Signals must be a set.")
    if not isinstance(buses, set):
        raise Exception(f"Buses must be a set.")
    if not isinstance(sequences, list):
        raise Exception(f"Sequences must be a list.")
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"Show progress bars option must be a boolean.")

    from typing import ContextManager

    # The progress bar wrapper.
    def progress_bar(iterable: Iterable) -> ContextManager:
        from vcdutilities import get_progress_bar_wrapper
        pbw = get_progress_bar_wrapper(show_progress_bars)
        return pbw(iterable, desc="Sequences searched from time ranges")

    from re import compile

    findings_by_sequence: Dict[str, Any] = {}
    for sequence in progress_bar(sequences):
        pattern = compile(r"({sequence})".format(sequence=sequence))

        findings_by_sequence[sequence] = {}

        findings_by_signals: Dict[str, Any] = {}
        for signal in signals:
            if signal.binary:
                if len(sequence) <= len(signal.binary):
                    if sequence in signal.binary:
                        findings_in_signal = {"findings": []}
                        if signal.timing:
                            offset_int = signal.timing.offset
                            clock_period_int = signal.timing.clock_period.number
                            findings_in_signal["offset"] = f"{signal.timing.offset} {signal.timing.clock_period.unit}"
                            findings_in_signal["clock_period"] = f"{signal.timing.clock_period.number} {signal.timing.clock_period.unit}"
                        else:
                            offset_int = 0
                            clock_period_int = 1
                            findings_in_signal["offset"] = "0"
                            findings_in_signal["clock_period"] = "unknown"
                        # Search from all substrings.
                        # To avoid re-evaluating the dot-expression.
                        pattern_match = pattern.match
                        binary = signal.binary
                        for start_index in range(len(binary)):
                            match = pattern_match(binary, start_index)
                            if match:
                                index0, index1 = match.span()
                                instant0 = index0 * clock_period_int + offset_int
                                instant1 = index1 * clock_period_int + offset_int
                                findings_in_signal["findings"].append((instant0, instant1))
                        findings_by_signals[signal.name.with_bus_description()] = findings_in_signal
        findings_by_sequence[sequence]["signals"] = findings_by_signals

        findings_by_buses: Dict[str, Any] = {}
        for bus in buses:
            findings_in_bus: Dict[str, Any] = {}
            for signal in bus.signals.values():
                if signal.binary:
                    # To avoid re-evaluating the dot-expression.
                    binary = signal.binary
                    if len(sequence) <= len(binary):
                        if sequence in binary:
                            findings_in_signal: Dict[str, Any] = {"findings": []}
                            if signal.timing:
                                offset_int = signal.timing.offset
                                clock_period_int = signal.timing.clock_period.number
                                findings_in_signal["offset"] = f"{signal.timing.offset} {signal.timing.clock_period.unit}"
                                findings_in_signal["clock_period"] = f"{signal.timing.clock_period.number} {signal.timing.clock_period.unit}"
                            else:
                                offset_int = 0
                                clock_period_int = 1
                                findings_in_signal["offset"] = "0"
                                findings_in_signal["clock_period"] = "unknown"

                            if len(sequence) == 1:
                                # Easy case.
                                for match in pattern.finditer(binary):
                                    index0, index1 = match.span()
                                    instant0 = index0 * clock_period_int + offset_int
                                    instant1 = index1 * clock_period_int + offset_int
                                    findings_in_signal["findings"].append((instant0, instant1))
                            else:
                                # Hard case. Search from all substrings.
                                # To avoid re-evaluating the dot-expression.
                                pattern_match = pattern.match
                                for start_index in range(len(binary)):
                                    match = pattern_match(binary, start_index)
                                    if match:
                                        index0, index1 = match.span()
                                        instant0 = index0 * clock_period_int + offset_int
                                        instant1 = index1 * clock_period_int + offset_int
                                        findings_in_signal["findings"].append((instant0, instant1))
                            findings_in_bus[signal.name.with_bus_description()] = findings_in_signal
            findings_by_buses[bus.name.with_range_description()] = findings_in_bus
        findings_by_sequence[sequence]["buses"] = findings_by_buses

    result.time_range_findings = findings_by_sequence

# -----------------------------------------------------------------------------
# The search function.
# -----------------------------------------------------------------------------


def execute_search(
        vcd: VCDObject,
        sequences: List[str],
        show_progress_bars: bool) -> SearchResults:
    """
    Search the target sequences from the binarized signals and buses.

    Parameters
    ----------
    vcd: VCDObject
        TODO
    sequences: List[str]
        TODO
    show_progress_bars: bool
        TODO

    Returns
    -------
    SearchResults
        TODO
    """

    result = SearchResults()

    # Pack the arguments to declutter the code.
    args = (result, vcd.signals, vcd.buses, sequences, show_progress_bars)

    logging.info(f"Searching from time instants.")
    find_sequences_from_time_instants(*args)

    logging.info(f"Searching from time ranges.")
    find_sequences_from_time_ranges(*args)

    return result

# -----------------------------------------------------------------------------
# Reporting functions.
# -----------------------------------------------------------------------------


def write_search_results_to_json_file(
        path: pathlib.Path,
        results: SearchResults):

    import json

    # Construct a dictionary tree from the search results.
    root = vars(results)

    with path.open("w") as file_handle:
        json.dump(root, file_handle)


def write_search_results_to_xml_file(
        path: pathlib.Path,
        results: SearchResults):

    from xml.etree.ElementTree import Element, SubElement, tostring

    data = vars(results)
    root = Element("search_report")

    tif_item = SubElement(root, "time_instant_findings")
    for sequence, finding_groups in data["time_instant_findings"].items():
        sequence_item = SubElement(tif_item, "sequence", value=str(sequence))
        if "signals" in finding_groups.keys():
            signals_item = SubElement(sequence_item, "signals")
            for name, findings_in_signal in finding_groups["signals"].items():
                offset = findings_in_signal["offset"]
                clock_period = findings_in_signal["clock_period"]
                signal_item = SubElement(signals_item, "signal", name=name, offset=str(offset), clock_period=str(clock_period))
                signal_item.text = " ".join([str(item) for item in findings_in_signal["findings"]])
        if "buses" in finding_groups.keys():
            buses_item = SubElement(sequence_item, "buses")
            for name, findings_in_bus in finding_groups["buses"].items():
                offset = findings_in_bus["offset"]
                clock_period = findings_in_bus["clock_period"]
                bus_item = SubElement(buses_item, "bus", name=name, offset=str(offset), clock_period=str(clock_period))
                for finding in findings_in_bus["findings"]:
                    for instant, signal_indices in finding.items():
                        instant_item = SubElement(bus_item, "time_instant", value=str(instant))
                        for indexgroup in signal_indices:
                            if isinstance(indexgroup, int):
                                indexgroup_item = SubElement(instant_item, "signal_index")
                                indexgroup_item.text = str(indexgroup)
                            else:
                                indexgroup_item = SubElement(instant_item, "signal_indices")
                                indexgroup_item.text = " ".join([str(index) for index in indexgroup])

    trf_item = SubElement(root, "time_range_findings")
    for sequence, finding_groups in data["time_range_findings"].items():
        sequence_item = SubElement(trf_item, "sequence", value=str(sequence))
        for name, findings_in_signal in finding_groups["signals"].items():
            offset = findings_in_signal["offset"]
            clock_period = findings_in_signal["clock_period"]
            signal_item = SubElement(sequence_item, "signal", name=str(name), offset=str(offset), clock_period=str(clock_period))
            for instant_range in findings_in_signal["findings"]:
                start, end = instant_range
                instant_item = SubElement(signal_item, "time_range", start=str(start), end=str(end))

        for name, findings_in_bus in finding_groups["buses"].items():
            bus_item = SubElement(sequence_item, "bus", name=str(name))
            for signal_name, findings_in_signal in findings_in_bus.items():
                offset = findings_in_signal["offset"]
                clock_period = findings_in_signal["clock_period"]
                signal_item = SubElement(bus_item, "signal", name=str(signal_name), offset=str(offset), clock_period=str(clock_period))
                for instant_range in findings_in_signal["findings"]:
                    start, end = instant_range
                    instant_item = SubElement(signal_item, "time_range", start=str(start), end=str(end))

    xmldata = tostring(root)
    with path.open("wb") as file_handle:
        file_handle.write(xmldata)

# =============================================================================
# Script execution interface.
# =============================================================================


if __name__ == "__main__":
    from vcdutilities import check_user_python_version
    check_user_python_version()

    import pickle
    import pathlib
    import argparse

    cmdparser = argparse.ArgumentParser(
        prog=f"vcdsearcher",
        usage=None,
        description=f"TODO",
        epilog=f"TODO",
        parents=[],
        formatter_class=argparse.HelpFormatter,
        prefix_chars="-",
        fromfile_prefix_chars=None,
        argument_default=None,
        conflict_handler="error",
        add_help=True,
        allow_abbrev=True,
        exit_on_error=True
    )

    # --------------------
    # Mandatory arguments.
    # --------------------

    cmdparser.add_argument(
        "input",
        type=pathlib.Path,
        help="Full path to the binarized VCD object file."
    )
    cmdparser.add_argument(
        "targets-file",
        type=pathlib.Path,
        help="Full path to a file which contains the target sequences."
    )
    cmdparser.add_argument(
        "outputs",
        nargs="*",
        type=pathlib.Path,
        help="List of full paths to the file where the search results are "
             "written to serialized to."
    )

    # -----------------------
    # User interface options.
    # -----------------------

    cmdparser.add_argument(
        "-wptc", "--write-progress-to-console",
        action="store_true",
        help="Let the script to report it's progress to the console."
    )
    cmdparser.add_argument(
        "-spb", "--show-progress-bars",
        action="store_true",
        help="Let the script to show it's parsing progress with progress bar."
    )

    args = vars(cmdparser.parse_args())

    # ------------------------------
    # Mandatory arguments unpacking.
    # ------------------------------

    input_file = args.get("input", None)
    if not isinstance(input_file, pathlib.Path):
        raise Exception(f"The input file must be a file.")
    if not input_file.exists():
        raise Exception(f"The input file must exist.")
    if not input_file.is_file():
        raise Exception(f"The input file must be a file.")
    if input_file.suffix != ".pickle":
        raise Exception(f"The input file extension must be .pickle.")

    targets_file = args.get("targets-file", None)
    if not isinstance(targets_file, pathlib.Path):
        raise Exception(f"The targets file must be a file.")
    if not targets_file.exists():
        raise Exception(f"The targets file must exist.")
    if not targets_file.is_file():
        raise Exception(f"The targets file must be a file.")
    if targets_file.suffix != ".txt":
        raise Exception(f"The targets file extension must be .txt.")

    # Get the target sequences from the file.
    targets: List[str] = []
    with targets_file.open("r") as targets_stream:
        while True:
            line = targets_stream.readline()
            if not line:
                # End of the file.
                break

            # Remove white space.
            line = line.strip()

            # Exclude empty lines.
            if line == "":
                continue

            # Exclude lines that begin with a number sign.
            if line[0] == "#":
                continue

            # Validate the target format.
            for symbol in line:
                if symbol not in SCALAR_DIGITS:
                    raise Exception(f"Target sequence must only contain 0, 1, "
                                    "x, X, z or Z.")

            targets.append(line)

    output_files = args.get("outputs", None)
    if output_files:
        for path in output_files:
            if not isinstance(path, pathlib.Path):
                raise Exception(f"The report file must be a file.")
            if not path.exists():
                raise Exception(f"The report file must exist.")
            if not path.is_file():
                raise Exception(f"The report file must be a file.")
            if path.suffix not in {".json", ".xml"}:
                raise Exception(f"The report file extension must be .json or "
                                ".xml.")
    else:
        raise Exception(f"You must give report files.")

    # ---------------------------------
    # User interface options unpacking.
    # ---------------------------------

    write_progress_to_console = args.get("write_progress_to_console", False)
    if not isinstance(write_progress_to_console, bool):
        raise Exception(f"The write progress to console option must be a boolean.")

    if write_progress_to_console:
        # The user wants to read scipt progress reports.
        logging.basicConfig(level=logging.INFO)
    else:
        # Report only if something goes wrong.
        logging.basicConfig(level=logging.WARNING)

    show_progress_bars = args.get("show_progress_bars", False)
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"The show progress bars option must be a boolean.")

    # -----------------
    # The search phase.
    # -----------------

    logging.info(f"Deserializing the binarized VCD object file.")
    with input_file.open("rb") as input_stream:
        parser_state = pickle.load(input_stream)
    logging.info(f"Deserialization finished.")

    logging.info(f"Searching the target sequences from the VCD object.")
    search_args = (parser_state.vcd_object, targets, show_progress_bars)
    results = execute_search(*search_args)
    logging.info(f"Search finished.")

    logging.info(f"Writing the search report(s).")
    for path in output_files:
        reporting_args = (path, results)
        if path.suffix == ".json":
            write_search_results_to_json_file(*reporting_args)
        elif path.suffix == ".xml":
            write_search_results_to_xml_file(*reporting_args)

    logging.info(f"Program finished.")
