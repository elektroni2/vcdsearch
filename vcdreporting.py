"""
# ! TODO: this module might be deprecated

Functions to perform reporting.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import pathlib
import logging

from typing import Dict, Sequence, Union, Optional, List

# =============================================================================
# Local module imports
# =============================================================================

from vcdbus import Bus
from vcdsignal import Signal
from vcdparser import ParserState
from vcdobject import VCDObject
from vcdsearcher import SearchResults

# =============================================================================
# Function definitions
# =============================================================================


def report_parsing_results(
        state: ParserState,
        write_to_console: bool,
        paths: Optional[List[pathlib.Path]]) -> None:
    """
    Write the parsing report to given location.

    Parameters
    ----------
    state: ParserState
        The state of the VCD parser after the parsing.
    write_to_console: bool
        True if the report is written to console.
    path: pathlib.Path
        The full path to the file where the report is written to.

    Raises
    ------
    TODO
    """

    # To simplify the code.
    vcd = state.vcd_object

    # Unpack the VCD information.

    date = vcd.simulation_date.date \
        if vcd.simulation_date \
        else None

    version = vcd.simulation_version.version \
        if vcd.simulation_version \
        else None
    task = vcd.simulation_version.system_task \
        if vcd.simulation_version \
        else None

    scale_multiplier = vcd.simulation_time_scale.number \
        if vcd.simulation_time_scale \
        else None
    scale_unit = vcd.simulation_time_scale.unit \
        if vcd.simulation_time_scale \
        else None

    comment = vcd.simulation_comment.comment \
        if vcd.simulation_comment \
        else None

    data = dict()
    data["date"] = date
    data["version"] = version
    data["task"] = task
    data["scale_multiplier"] = scale_multiplier
    data["scale_unit"] = repr(scale_unit)
    data["comment"] = comment

    variable_count = len(state.vcd_object.objects)
    bus_count = len(state.vcd_object.buses)
    signal_count = len(state.vcd_object.signals)

    data["variable_count"] = variable_count
    data["bus_count"] = bus_count
    data["signal_count"] = signal_count

    text: Dict[str, str] = dict()

    text["date"] = f"Simulation date: {date}" if date \
        else f"The VCD file did not declare the simulation date."

    text["version"] = f"Simulation version: {version}" if version \
        else f"The VCD file did not declare the simulation version."

    text["task"] = f"Simulation system task: {task}" if task \
        else f"The VCD file did not declare the simulation system task."

    text["scale_multiplier"] = f"Simulation time multiplier: {scale_multiplier}" if scale_multiplier \
        else f"The VCD file did not declare the simulation time scale."

    text["scale_unit"] = f"Simulation time unit: {scale_unit}" if scale_unit \
        else f"The VCD file did not declare the simulation time scale."

    text["comment"] = f"Simulation comment:\n{comment}" if comment \
        else f"The VCD file did not declare the simulation comment."

    text["variable_count"] = f"Simulation variable count: {variable_count}"
    text["bus_count"] = f"Simulation bus count: {bus_count}"
    text["signal_count"] = f"Simulation signal count: {signal_count}"

    if write_to_console:
        logging.info(text["date"])
        logging.info(text["version"])
        logging.info(text["task"])
        logging.info(text["scale_multiplier"])
        logging.info(text["scale_unit"])
        logging.info(text["comment"])
        logging.info(text["variable_count"])
        logging.info(text["bus_count"])
        logging.info(text["signal_count"])

    if not paths:
        return

    for path in paths:

        # ! this can raise OSError

        if path.suffix == ".txt":
            with path.open("w") as file_handle:
                file_handle.write(text["date"] + "\n")
                file_handle.write(text["version"] + "\n")
                file_handle.write(text["task"] + "\n")
                file_handle.write(text["scale_multiplier"] + "\n")
                file_handle.write(text["scale_unit"] + "\n")
                file_handle.write(text["comment"] + "\n")
                file_handle.write(text["variable_count"] + "\n")
                file_handle.write(text["bus_count"] + "\n")
                file_handle.write(text["signal_count"] + "\n")

        elif path.suffix == ".json":
            import json

            with path.open("w") as file_handle:
                json.dump(data, file_handle)

        elif path.suffix == ".xml":
            from xml.etree.ElementTree import Element, SubElement, tostring

            root = Element("ParsingReport")

            for name, value in data.items():
                item = SubElement(root, name)
                item.set("name", name)
                if not value:
                    value = "None"
                item.set("value", str(value))
                item.text = text[name]

            xml_data = tostring(root)
            with path.open("wb") as file_handle:
                file_handle.write(xml_data)

        elif path.suffix == ".csv":
            import csv

            fields = data.keys()

            with path.open("w") as file_handle:
                writer = csv.DictWriter(file_handle, fieldnames=fields)
                writer.writeheader()
                writer.writerow(data)


def write_binarization_report_to_json_file(path: pathlib.Path, vcd: VCDObject):
    import json

    data: Dict[str, Dict] = dict()

    data["signals"] = dict()
    for signal in vcd.signals:
        name = signal.name.with_bus_description
        data["signals"][name] = signal.to_dictionary()

    data["buses"] = dict()
    for bus in vcd.buses:
        name = bus.name.with_range_description
        data["buses"][name] = bus.to_dictionary()

    with path.open("w") as file_handle:
        json.dump(data, file_handle)


def write_binarization_report_to_xml_file(path: pathlib.Path, vcd: VCDObject):
    from xml.etree.ElementTree import Element, SubElement, tostring

    root = Element("binarization_report")

    signals_item = SubElement(root, "signals")
    for signal in vcd.signals:
        signal_data = signal.to_dictionary()
        signal_binary = signal_data.pop("binary")
        signal_item = SubElement(signals_item, "signal", attrib=signal_data)
        signal_item.text = signal_binary

    buses_item = SubElement(root, "buses")
    for bus in vcd.buses:
        bus_data = bus.to_dictionary()
        bus_signals = bus_data.pop("signals")
        bus_item = SubElement(buses_item, "bus", attrib=bus_data)
        for signal_data in bus_signals.values():
            signal_binary = signal_data.pop("binary")
            signal_item = SubElement(bus_item, "signal", attrib=signal_data)
            signal_item.text = signal_binary

    data = tostring(root)
    with path.open("wb") as file_handle:
        file_handle.write(data)


def report_binarization_results(
        vcd: VCDObject,
        write_to_console: bool,
        paths: Optional[List[pathlib.Path]]) -> None:
    """
    Write the binarization results to given location.

    TODO
    """

    # ! this can raise OSError

    if not paths:
        return

    for path in paths:
        if path.suffix == ".json":
            write_binarization_report_to_json_file(path, vcd)

        elif path.suffix == ".xml":
            write_binarization_report_to_xml_file(path, vcd)


def write_search_results_to_json_file(
        path: pathlib.Path,
        results: SearchResults):

    import json

    # Construct a dictionary tree from the search results.
    root = {}
    for super_name, supergroup in vars(results).items():
        super_data = {}
        for sub_name, subgroup in supergroup.items():
            sub_data = {}
            for sequence, sequence_group in subgroup.items():
                sequence_data = {}
                for finding in sequence_group:
                    finding_data = {}
                    finding_data["name"] = str(finding.target_object)
                    finding_data["instants"] = finding.time_instants
                    finding_data["ranges"] = \
                        finding.time_instant_ranges
                    sequence_data[finding_data["name"]] = finding_data
                sub_data[sequence] = sequence_data
            super_data[sub_name] = sub_data
        root[super_name] = super_data

    with path.open("w") as file_handle:
        json.dump(root, file_handle)


def write_search_results_to_xml_file(
        path: pathlib.Path,
        results: SearchResults):

    from xml.etree.ElementTree import Element, SubElement, tostring

    root = Element("search_report")

    tif_item = SubElement(root, "time_instant_findings")
    for tif_type, tif_findings_object in results.time_instant_findings.items():
        tif_type_item = SubElement(tif_item, tif_type)
        for sequence, findings in tif_findings_object.items():
            sequence_item = SubElement(tif_type_item, "sequence", value=str(sequence))
            for finding in findings:
                target = finding.target_object
                target_type = target.__class__.__name__.lower()
                target_name = str(target)
                instant_offset = str(target.timing.offset)
                instant_multiplier = str(target.timing.clock_period.number)
                instant_unit = target.timing.clock_period.unit.to_string()
                instants = " ".join([str(instant) for instant in finding.time_instants])
                signal_item = SubElement(sequence_item, target_type,
                    name=target_name,
                    offset=instant_offset,
                    multiplier=instant_multiplier,
                    unit=instant_unit)
                signal_item.text = instants

    # TODO!!!
    trf_item = SubElement(root, "time_range_findings")
    for trf_type, trf_findings_object in results.time_range_findings.items():
        trf_type_item = SubElement(trf_item, trf_type)
        for sequence, findings in trf_findings_object.items():
            sequence_item = SubElement(trf_type_item, "sequence", value=str(sequence))
            for finding in findings:
                finding_item = SubElement(sequence_item, "signal", name=str(finding.target_object))
                for pair in finding.time_instant_ranges:
                    start, end = pair
                    range_item = SubElement(finding_item, "finding", start=str(start), end=str(end))

    xmldata = tostring(root)
    with path.open("wb") as file_handle:
        file_handle.write(xmldata)


def report_search_results(
        results: SearchResults,
        write_to_console: bool,
        paths: Optional[List[pathlib.Path]]) -> None:
    """
    Write the search results to given location.

    TODO
    """

    if write_to_console:
        #!raise NotImplementedError()
        pass

    if not paths:
        return

    for path in paths:

        # ! this can raise OSError
        if path.suffix == ".txt":
            pass
            # with path.open("w") as file_handle:
            # file_handle.writelines(table)
            # raise NotImplementedError()

        elif path.suffix == ".json":
            write_search_results_to_json_file(path, results)

        elif path.suffix == ".xml":
            write_search_results_to_xml_file(path, results)

        elif path.suffix == ".csv":
            pass
            # raise NotImplementedError()


def report(
        results: Union[ParserState, VCDObject, SearchResults],
        path: pathlib.Path) -> None:
    """
    TODO
    """

    data = None
    if isinstance(results, ParserState):
        pass
    elif isinstance(results, VCDObject):
        pass
    elif isinstance(results, SearchResults):
        pass

    if not path:
        return

    if path.suffix == ".txt":
        pass

    elif path.suffix == ".json":
        pass

    elif path.suffix == ".xml":
        pass

    elif path.suffix == ".csv":
        pass
