"""
Test cases for the utilities.py module.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import unittest
import pathlib

# =============================================================================
# Local module imports
# =============================================================================

from vcdutilities import \
    remove_duplicates, \
    verify_that_path_points_to_file, \
    verify_that_path_points_to_file_with_extension, \
    PathDoesNotExist, \
    PathDoesNotPointToFile, \
    IllegalFileExtension

# =============================================================================
# Test case definitions
# =============================================================================


class TestRemoveDuplicates(unittest.TestCase):
    def test_list_is_none(self):
        """
        Test that it raises an exception if the list is none.
        """
        with self.assertRaises(Exception):
            _result = remove_duplicates(None)

    def test_list_with_no_duplicates(self):
        """
        Test that the result is correct when there are no duplicates.
        """
        result = remove_duplicates(["a", 1, 0.101])
        self.assertEqual(result, ["a", 1, 0.101])

    def test_list_with_duplicates(self):
        """
        Test that the result is correct when there are duplicates.
        """
        result = remove_duplicates(["a", "a", 1, 0.101, 0.101, 1, "a"])
        self.assertEqual(result, ["a", 1, 0.101])


class TestVerifyThatPathPointsToFile(unittest.TestCase):
    def test_path_is_none(self):
        """
        Test that it raises an exception if no path is given.
        """
        with self.assertRaises(PathDoesNotExist):
            verify_that_path_points_to_file(None)

    def test_path_is_random_string(self):
        """
        Test that it raises an exception if the path is just a random string.
        """
        with self.assertRaises(PathDoesNotExist):
            verify_that_path_points_to_file("random string")

    def test_path_does_not_exist(self):
        """
        Test that it raises an exception if the path does not exist.
        """
        path = pathlib.Path("whatever//whatever")
        with self.assertRaises(PathDoesNotExist):
            verify_that_path_points_to_file(path)

    def test_path_is_directory(self):
        """
        Test that it raises an exception if the path points to a directory.
        """
        path = pathlib.Path.cwd()
        with self.assertRaises(PathDoesNotPointToFile):
            verify_that_path_points_to_file(path)


# ? deprecated?
class TestVerifyThatPathPointsToFileWithExtension(unittest.TestCase):
    def atest_file_has_wrong_extension(self):
        """
        Test that it raises an exception if the file extension is incorrect.
        """
        path = pathlib.Path.cwd()
        path = path.joinpath("tests//test_inputvalidator.py")
        with self.assertRaises(IllegalFileExtension):
            verify_that_path_points_to_file_with_extension(path, {".txt"})

    def atest_file_has_correct_extension(self):
        """
        Test that it does not raise an exception if the file extension is
        correct.
        """
        path = pathlib.Path.cwd()
        path = path.joinpath("tests//test_inputvalidator.py")
        try:
            verify_that_path_points_to_file_with_extension(path, {".py"})
            self.assertTrue(True)
        except Exception:
            self.assertTrue(False)
