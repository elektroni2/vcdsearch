"""
Test cases for the bin2vcd.py module.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import unittest

from io import StringIO
from vcdtimescaleunit import TimeScaleUnit
from vcdtimescale import TimeScale

# =============================================================================
# Local module imports
# =============================================================================

from bin2vcd import \
    NegativeOffset, \
    NegativeClockPeriod, \
    execute_transformation, \
    parse_input_stream, \
    transform_string_to_time_value_pairs as str2tv

# =============================================================================
# Test case definitions
# =============================================================================


class TestTransformStringToTimeValuePairs(unittest.TestCase):
    def test_negative_offset(self):
        """
        Test that a negative offset raises an exception.
        """
        with self.assertRaises(NegativeOffset):
            offset = TimeScale(-1, TimeScaleUnit.FEMTOSECOND)
            clock_period = TimeScale(100, TimeScaleUnit.FEMTOSECOND)
            _ = str2tv("0", offset, clock_period)

    def test_negative_clock_period(self):
        """
        Test that a negative clock period raises an exception.
        """
        with self.assertRaises(NegativeClockPeriod):
            offset = TimeScale.zero()
            clock_period = TimeScale(-100, TimeScaleUnit.FEMTOSECOND)
            _ = str2tv("0", offset, clock_period)

    def test_zero_clock_period(self):
        """
        Test that a zero as a clock period raises an exception.
        """
        with self.assertRaises(NegativeClockPeriod):
            offset = TimeScale.zero()
            clock_period = TimeScale.zero()
            _ = str2tv("0", offset, clock_period)

    def test_constant_zero(self):
        offset = TimeScale.zero()
        clock_period = TimeScale(100, TimeScaleUnit.FEMTOSECOND)
        result = str2tv("0", offset, clock_period)
        self.assertEqual(result, {0: "0"})

    def test_constant_one(self):
        offset = TimeScale.zero()
        clock_period = TimeScale(100, TimeScaleUnit.FEMTOSECOND)
        result = str2tv("1", offset, clock_period)
        self.assertEqual(result, {0: "1"})

    def test_value_changes_once(self):
        offset = TimeScale.zero()
        clock_period = TimeScale(100, TimeScaleUnit.FEMTOSECOND)
        result = str2tv("01", offset, clock_period)
        expected = {
            0: "0",
            100: "1"
        }
        self.assertEqual(result, expected)

    def test_value_changes_multiple_times(self):
        offset = TimeScale.zero()
        clock_period = TimeScale(100, TimeScaleUnit.FEMTOSECOND)
        result = str2tv("0101", offset, clock_period)
        excepted = {
            0: "0",
            100: "1",
            200: "0",
            300: "1"
        }
        self.assertEqual(result, excepted)

    def test_value_stays_same(self):
        offset = TimeScale.zero()
        clock_period = TimeScale(100, TimeScaleUnit.FEMTOSECOND)
        result = str2tv("0110", offset, clock_period)
        expected = {
            0: "0",
            100: "1",
            300: "0"
        }
        self.assertEqual(result, expected)

    def test_non_zero_offset(self):
        offset = TimeScale(13, TimeScaleUnit.FEMTOSECOND)
        clock_period = TimeScale(100, TimeScaleUnit.FEMTOSECOND)
        result = str2tv("0110", offset, clock_period)
        excepted = {
            13: "0",
            113: "1",
            313: "0"
        }
        self.assertEqual(result, excepted)


class TestExecuteTransformation(unittest.TestCase):
    def test_targets_is_none(self):
        with self.assertRaises(Exception):
            execute_transformation(None, False)

    def test_targets_is_empty(self):
        result = execute_transformation({}, False)
        result.seek(0)
        result_str = result.read()
        self.assertEqual(result_str, "")

    def test_simple_signal(self):
        stream = StringIO("mysignal 100ns 01")
        targets = parse_input_stream(stream)
        expected = """\
$timescale
    1ns
$end
$var wire 1 0 mysignal $end
$enddefinitions $end
#0
00
#100
10
"""
        result = execute_transformation(targets, False)
        result.seek(0)
        result_str = result.read()
        self.assertEqual(result_str, expected)

    def test_scopes_and_signals(self):
        obj0 = "toplvl.sublvl.mysignal0 100ns 01"
        obj1 = "toplvl.mysignal1 100ns 10"
        obj2 = "toplvl.otherlvl.mysignal2 100ns 01"
        obj3 = "mysignal3 100ns 10"
        objects = [obj0, obj1, obj2, obj3]
        stream = StringIO("\n".join(objects))
        targets = parse_input_stream(stream)
        expected = """\
$timescale
    1ns
$end
$var wire 1 3 mysignal3 $end
$scope module toplvl $end
$var wire 1 1 mysignal1 $end
$scope module otherlvl $end
$var wire 1 2 mysignal2 $end
$scope module sublvl $end
$var wire 1 0 mysignal0 $end
$upscope $end
$upscope $end
$upscope $end
$enddefinitions $end
#0
00
11
02
13
#100
10
01
12
03
"""
        result = execute_transformation(targets, False)
        result.seek(0)
        result_str = result.read()
        #print(expected)
        #print(result_str)
        self.assertEqual(result_str, expected)

    def test_scopes_and_buses(self):
        obj0 = "toplvl.sublvl.mybus0[1:0] 100ns 01;10"
        obj1 = "toplvl.mybus1[1:0] 100ns 10;01"
        obj2 = "toplvl.otherlvl.mybus2[1:0] 100ns 01;10"
        obj3 = "mybus3[1:0] 100ns 10;01"
        objects = [obj0, obj1, obj2, obj3]
        stream = StringIO("\n".join(objects))
        targets = parse_input_stream(stream)
        expected = """\
$timescale
    1ns
$end
$var wire 1 6 mybus3[0] $end
$var wire 1 7 mybus3[1] $end
$scope module toplvl $end
$var wire 1 2 mybus1[0] $end
$var wire 1 3 mybus1[1] $end
$scope module otherlvl $end
$var wire 1 4 mybus2[0] $end
$var wire 1 5 mybus2[1] $end
$scope module sublvl $end
$var wire 1 0 mybus0[0] $end
$var wire 1 1 mybus0[1] $end
$upscope $end
$upscope $end
$upscope $end
$enddefinitions $end
#0
00
11
12
03
04
15
16
07
#100
10
01
02
13
14
05
06
17
"""
        result = execute_transformation(targets, False)
        result.seek(0)
        result_str = result.read()
        #print(expected)
        #print(result_str)
        self.assertEqual(result_str, expected)


class TestParseInputStream(unittest.TestCase):
    def test_stream_is_none(self):
        with self.assertRaises(Exception):
            _ = parse_input_stream(None)

    def test_stream_is_empty(self):
        stream = StringIO("")
        result = parse_input_stream(stream)
        expected = {}
        self.assertEqual(expected, result)

    def test_simple_signals(self):
        stream = StringIO("mysignal1 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("_mysignal_1_ 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "_mysignal_1_": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mysignal1 100 ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mysignal1 100  ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mysignal1 100s 0101")
        result = parse_input_stream(stream)
        expected = {
            "mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.SECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("toplvl.mysignal1 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "toplvl.mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("toplvl.sublvl_._mysignal1 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "toplvl.sublvl_._mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mysignal1 100ns 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mysignal1 100ns 100 ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mysignal1 100 ns 100 ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mysignal1": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            }
        }
        self.assertEqual(expected, result)

    def test_simple_buses(self):
        stream = StringIO("mybus[0] 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mybus[0]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mybus [0] 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mybus[0]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mybus [0] 100 ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mybus[0]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mybus[1:0] 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mybus[0]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            },
            "mybus[1]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("mybus[1:0] 120 ns 100 ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mybus[0]": {
                "clock_period": TimeScale(120, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            },
            "mybus[1]": {
                "clock_period": TimeScale(120, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            }
        }
        self.assertEqual(expected, result)

    def test_double_colon_notation(self):
        stream = StringIO("mybus[1::0] 100ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "mybus[0]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            },
            "mybus[1]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": None
            }
        }
        self.assertEqual(expected, result)

        stream = StringIO("toplvl.mybus[1::0] 100 ns 100 ns 0101")
        result = parse_input_stream(stream)
        expected = {
            "toplvl.mybus[0]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            },
            "toplvl.mybus[1]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            }
        }
        self.assertEqual(expected, result)

    def test_stream_has_wrong_format(self):
        stream = StringIO("justname")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

        stream = StringIO("mysignal 100")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

        stream = StringIO("justname 100 1010")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

        stream = StringIO("justname 100ns 1234")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

        stream = StringIO("justname[ 100ns 0101")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

        stream = StringIO("justname] 100ns 0101")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

        stream = StringIO("justname[] 100ns 0101")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

        stream = StringIO("justname[a] 100ns 0101")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

        stream = StringIO("justname[:] 100ns 0101")
        with self.assertRaises(Exception):
            _ = parse_input_stream(stream)

    def test_multiple_targets(self):
        stream = StringIO(
            "mysignal 100 ns 100 ns 0101\n"
            "toplvl.sublvl.mybus[1::0] 100 ns 100 ns 0101\n"
            "toplvl.mybus[1:0] 100 ns 100 ns 01101;11101\n"
        )
        result = parse_input_stream(stream)
        expected = {
            "mysignal": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            },
            "toplvl.sublvl.mybus[0]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            },
            "toplvl.sublvl.mybus[1]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "0101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            },
            "toplvl.mybus[0]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "01101;11101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            },
            "toplvl.mybus[1]": {
                "clock_period": TimeScale(100, TimeScaleUnit.NANOSECOND),
                "string": "01101;11101",
                "offset": TimeScale(100, TimeScaleUnit.NANOSECOND)
            }
        }
        self.assertEqual(expected, result)
