"""
Test cases for the vcdparser.py module.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import re
import string
import logging
import unittest
import itertools

from io import StringIO

# =============================================================================
# Local module imports
# =============================================================================

from vcdparser import \
    FileStreamNotTextIO, \
    InvalidDeclaration, \
    parse_vcd

# =============================================================================
# Test case definitions
# =============================================================================


# ? deprecated?
def parse_variable_reference():
    pass
class TestParseVariableReference(unittest.TestCase):
    def subtest_identifier_with_illegal_symbol(self, bad_identifier: str):
        """
        Test that an exception is raised if the identifier has a illegal
        symbol.
        """
        with self.assertRaises(InvalidVariableReference):
            _result = parse_variable_reference(bad_identifier)

    def atest_multiple_bad_identifiers(self):
        """
        Test that multiple identifiers with illegal symbols raise exceptions.
        """
        bad_identifiers = [
            1,
            "",
            " ",
            "!",
            "a!",
            "a a",
            "hello world!",
            "i like spaces",
            "  ",
        ]
        for identifier in bad_identifiers:
            self.subtest_identifier_with_illegal_symbol(identifier)


    def atest_identifier_with_spaces(self):
        """
        Test that an exception is raised if the identifier has spaces.
        """
        with self.assertRaises(InvalidVariableReference):
            _result = parse_variable_reference("Hello world")

    def atest_missing_bracket_left(self):
        """
        Test that an exception is raised if a left bracket is missing.
        """
        with self.assertRaises(InvalidVariableReference):
            _result = parse_variable_reference("whatever 1]")

    def atest_missing_bracket_right(self):
        """
        Test that an exception is raised if a right bracket is missing.
        """
        with self.assertRaises(InvalidVariableReference):
            _result = parse_variable_reference("whatever [1")

    def atest_missing_colon(self):
        """
        Test that an exception is raised if a colon is missing.
        """
        with self.assertRaises(InvalidVariableReference):
            _result = parse_variable_reference("whatever [2 1]")

    def atest_exhaustive(self):
        """
        Test that TODO
        """

        # ! too expensive...

        return

        # Matches just bare identifier.
        type_1_expression = re.compile(r"^[a-zA-Z0-9]+\Z")

        # Matches identifier with bracketed index.
        type_2_expression = re.compile(r"^[a-zA-Z0-9]+ {1}\[{1}[0-9]+\]{1}\Z")

        # Matches identifier with bracketed range.
        type_3_expression = re.compile(r"^[a-zA-Z0-9]+ {1}\[{1}[0-9]+:{1}[0-9]+\]{1}\Z")

        # The minimum length for the longest variable reference type is 6
        # symbols.
        for reference in itertools.product(string.printable, repeat=6):
            reference = "".join(reference)

            is_type_1 = False
            type_1 = type_1_expression.match(reference)
            if type_1:
                is_type_1 = True

            is_type_2 = False
            type_2 = type_2_expression.match(reference)
            if type_2:
                is_type_2 = True

            is_type_3 = False
            type_3 = type_3_expression.match(reference)
            if type_3:
                is_type_3 = True

            succeeds = is_type_1 or is_type_2 or is_type_3

            if succeeds:
                try:
                    _result = parse_variable_reference(reference)
                    self.assertTrue(True)
                except Exception:
                    self.assertTrue(False)
            else:
                with self.assertRaises(InvalidVariableReference):
                    _result = parse_variable_reference(reference)

        return

        if 1==0:

            name_is_valid = False
            name = re.search("[a-zA-Z0-9]+ *", reference)
            if name:
                name_is_valid = True

            left_bracket_found = False


            try:
                _result = parse_variable_reference(reference)
                # Is the succeeding justified?
                result_justified = False
                pass
                self.assertTrue(True)
            except InvalidVariableReference as error:
                # Is the exception justified?
                result_justified = False
                if error.reference_is_not_string:
                    pass
                elif error.wrong_amount_of_parts:
                    pass
                elif error.wrong_amount_of_indices:
                    pass
                elif error.missing_bracket_left:
                    pass
                elif error.missing_bracket_right:
                    pass
                elif error.index_not_number:
                    pass
                elif error.index_not_number_left:
                    pass
                elif error.index_not_number_right:
                    pass
                elif error.indices_in_wrong_order:
                    pass
                elif error.name_not_alphanumeric:
                    result_justified = not reference.isalnum()

                self.assertTrue(result_justified)

        return

        # Let's assume that the identifier and indices are only 1 symbol.
        # Then we can only have 1, 4 and 6 as the lengths of the references.
        for length in [1, 4, 6]:
            for reference in itertools.product(string.printable, repeat=length):
                reference = "".join(reference)
                identifier_is_alphanumeric = reference[0].isalnum()

                if length == 1:
                    succeeds = identifier_is_alphanumeric
                elif length == 4:
                    has_left_bracket = reference[1] == "["
                    has_number_as_index = reference[2].isdigit()
                    has_right_bracket = reference[3] == "]"
                    succeeds = \
                        identifier_is_alphanumeric and \
                        has_left_bracket and \
                        has_number_as_index and \
                        has_right_bracket
                elif length == 6:
                    has_left_bracket = reference[1] == "["
                    has_number_as_left_index = reference[2].isdigit()
                    has_colon = reference[3] == ":"
                    has_number_as_right_index = reference[4].isdigit()
                    has_right_bracket = reference[5] == "]"
                    succeeds = \
                        identifier_is_alphanumeric and \
                        has_left_bracket and \
                        has_number_as_left_index and \
                        has_colon and \
                        has_number_as_right_index and \
                        has_right_bracket

                if succeeds:
                    try:
                        _result = parse_variable_reference(reference)
                        self.assertTrue(True)
                    except Exception:
                        self.assertTrue(False)
                else:
                    try:
                        with self.assertRaises(InvalidDeclaration):
                            _result = parse_variable_reference(reference)
                    except AssertionError as error:
                        print()
                        logging.exception(f"Test failed.")


class TestParseVCD(unittest.TestCase):
    def test_file_stream_is_none(self):
        with self.assertRaises(FileStreamNotTextIO):
            _ = parse_vcd(None, [], False)

    def test_file_stream_is_empty(self):
        stream = StringIO("")
        with self.assertRaises(Exception):
            _ = parse_vcd(stream, [], False)

    def test_file_stream_is_random_string(self):
        stream = StringIO("whatever")
        with self.assertRaises(Exception):
            _ = parse_vcd(stream, [], False)

    def test_simple1(self):
        timescale = "1 ps"
        comment = "whatever"
        text = f"""\
$timescale {timescale} $end
$enddefinitions $end
$comment {comment} $end
"""
        stream = StringIO(text)
        vcd = parse_vcd(stream, [], False).vcd_object
        self.assertEqual(str(vcd.simulation_time_scale), timescale)
        self.assertEqual(vcd.simulation_comment.comment, comment)

    def test_simple2(self):
        date = "31.12.2021"
        version = "handwritten"
        timescale = "1 ps"
        comment = "whatever"
        text = f"""\
$date {date} $end
$version {version} $end
$timescale {timescale} $end
$comment {comment} $end
$scope module toplvl $end
$var wire 1 a mysignal $end
$upscope $end
$enddefinitions $end
#0
0a
#100
1a
#200
0a
#300
1a
"""
        stream = StringIO(text)
        vcd = parse_vcd(stream, [], False).vcd_object
        self.assertEqual(vcd.simulation_date.date, date)
        self.assertEqual(vcd.simulation_version.version, version)
        self.assertEqual(str(vcd.simulation_time_scale), timescale)
        self.assertEqual(vcd.simulation_comment.comment, comment)

    def test_simple3(self):
        date = "31.12.2021"
        version = "handwritten"
        timescale = "1 ps"
        comment = "whatever"
        text = f"""\
$date {date} $end
$version {version} $end
$timescale {timescale} $end
$scope module toplvl $end
$var wire 1 a mysignal $end
$upscope $end
$enddefinitions $end
$comment {comment} $end
#0
0a
#100
1a
#200
0a
#300
1a
"""
        stream = StringIO(text)
        vcd = parse_vcd(stream, [], False).vcd_object
        self.assertEqual(vcd.simulation_date.date, date)
        self.assertEqual(vcd.simulation_version.version, version)
        self.assertEqual(str(vcd.simulation_time_scale), timescale)
        self.assertEqual(vcd.simulation_comment.comment, comment)
