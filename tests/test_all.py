"""
Test cases for all modules.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import difflib
import unittest

from io import StringIO
from pprint import pprint
from typing import List, Tuple
from pathlib import Path

# =============================================================================
# Local module imports
# =============================================================================

from bin2vcd import parse_input_stream, execute_transformation
from vcdobject import VCDObject
from vcdparser import ParserState, parse_vcd
from vcdtimescale import TimeScale
from vcdbinarizer import execute_binarization

# =============================================================================
# Test case definitions
# =============================================================================


class TestParsingAndBinarization(unittest.TestCase):
    def get_vcd_object(self, path: Path) -> Tuple[str, ParserState]:
        with path.open("r") as handle:
            text = handle.read()
            pstate = parse_vcd(handle, [], False)
        return (text, pstate)

    def make_string(self, vcdobj: VCDObject) -> List[str]:
        strings: List[str] = list()
        for signal in vcdobj.signals_list:
            name = signal.name.with_path()
            clock_period = signal.timing.clock_period
            offset = str(TimeScale(signal.timing.offset, clock_period.unit))
            clock_period = str(clock_period)
            binary = signal.binary
            strings.append(f"{name} {clock_period} {offset} {binary}")
        return "\n".join(strings)

    def make_text2(self, string: str) -> str:
        stream = StringIO(string)
        targets = parse_input_stream(stream)
        args = (targets, False)
        text2 = execute_transformation(*args)
        text2.seek(0)
        return text2.read()

    def transform_inverse_transform_comparison(self, path: str) -> bool:
        path = Path.cwd().joinpath(Path(path))
        text1, pstate = self.get_vcd_object(path)
        vcdobj = pstate.vcd_object
        args = (vcdobj, {}, False)
        execute_binarization(*args)
        string = self.make_string(vcdobj)
        text2 = self.make_text2(string)
        differences = difflib.context_diff(text1, text2)
        differences = list(differences)
        if len(differences) == 0:
            self.assertTrue(True)
        else:
            pprint(differences)  # ! not helpful, create better format
            self.assertTrue(False)

    def test_simple0(self):
        self.transform_inverse_transform_comparison("tests\\simple0.vcd")

    def test_simple1(self):
        self.transform_inverse_transform_comparison("tests\\simple1.vcd")

    def test_adder1(self):
        self.transform_inverse_transform_comparison("tests\\adder1.vcd")

    def test_adder2(self):
        self.transform_inverse_transform_comparison("tests\\adder2.vcd")
