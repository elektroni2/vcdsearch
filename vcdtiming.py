"""
TODO
"""

# =============================================================================
# Standard library imports
# =============================================================================

from dataclasses import dataclass

# =============================================================================
# Local module imports
# =============================================================================

from vcdtimescale import TimeScale

# =============================================================================
# Timing definition
# =============================================================================


@dataclass
class Timing:
    """
    TODO
    """

    # To save memory, to get faster attribute accesses and to deny the creation
    # of new attributes dynamically.
    __slots__ = [
        "offset",
        "clock_period"
    ]

    offset: int
    clock_period: TimeScale

    def __repr__(self) -> str:
        return \
            f"Offset is {self.offset} and clock period is {self.clock_period}"
