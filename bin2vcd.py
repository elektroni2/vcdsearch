"""
# TODO: update documentation

Functions to transform a strings of signal data to a VCD file.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import re
import pathlib
import logging

from io import StringIO, TextIOWrapper
from typing import Any, Deque, Dict, List, TextIO, Tuple

# =============================================================================
# Local module imports
# =============================================================================

from vcdtimescale import TimeScale
from vcdstringtools import get_signal_index
from vcdtimescaleunit import TimeScaleUnit

# =============================================================================
# Exception definitions
# =============================================================================


class NegativeOffset(Exception):
    """
    Raised when a offset is negative.
    """
    def __init__(self) -> None:
        self.message = f"Offset must be a zero or a positive TimeScale."
        super().__init__(self.message)


class NegativeClockPeriod(Exception):
    """
    Raised when a clock period is negative.
    """
    def __init__(self) -> None:
        self.message = f"Clock period must be a positive TimeScale larger " \
                        "than zero."
        super().__init__(self.message)

# =============================================================================
# Function definitions
# =============================================================================


def transform_string_to_time_value_pairs(
        string: str,
        offset: TimeScale,
        clock_period: TimeScale) -> Dict[int, str]:
    """
    TODO
    """

    if offset < TimeScale.zero():
        raise NegativeOffset()
    if clock_period <= TimeScale.zero():
        raise NegativeClockPeriod()

    # ! is this safe?
    offset_int = offset.number
    clock_period_int = clock_period.number

    result: Dict[int, str] = {}

    if len(string) == 0:
        pass
    elif len(string) == 1:
        result[offset_int] = string
    else:
        value_old = string[0]
        result[offset_int] = value_old
        for index in range(1, len(string)):
            value = string[index]
            if value != value_old:
                instant = clock_period_int * index + offset_int
                result[instant] = value
                value_old = value

    return result


def build_time_value_pairs(
        targets: Dict[str, Dict[str, Any]]) -> Tuple[
            Dict[str, List[str]], Dict[str, Any], TimeScaleUnit]:
    """
    TODO
    """

    if not isinstance(targets, dict):
        raise Exception(f"Targets must be a dictionary.")

    # A running counter for the identifier codes.
    identifier_code = 0

    # The time scale unit for the VCD file.
    # Start from the largest unit.
    smallest_time_unit = TimeScaleUnit.SECOND

    var_declarations = {
        "name": "root",
        "data": [],
        "children": {}
    }

    global_tv: Dict[str, List[str]] = {}
    for full_name, data in targets.items():
        # Get the hierarchy path (if exists) and the object name.
        parts = full_name.split(".")
        path = parts[:-1]
        name = parts[-1]

        # Add the variable to the variable declarations.

        # Find (or create) the node at the correct hierarchy level.
        current_level: Dict[str, Any] = var_declarations
        for level in path:
            if level in current_level["children"].keys():
                current_level = current_level["children"][level]
                continue
            else:
                current_level["children"][level] = {
                    "name": level,
                    "data": [],
                    "children": {}
                }
                current_level = current_level["children"][level]
        current_level["data"].append({
            "identifier_code": identifier_code,
            "reference": name
        })

        clock_period = data["clock_period"]
        offset = data["offset"]
        string = data["string"]

        if not offset:
            offset = TimeScale.zero()

        # Check if the unit for the time value change time change should be
        # updated.
        if clock_period.unit < smallest_time_unit:
            smallest_time_unit = clock_period.unit

        # Transform the binary string to the time value pairs representation.
        signal_index = get_signal_index(name)
        if signal_index is not None and ":" in name:
            string_parts = string.split(";")
            binary_string = string_parts[signal_index]
        else:
            binary_string = string
        args = (binary_string, offset, clock_period)
        tv = transform_string_to_time_value_pairs(*args)

        for instant_int, value in tv.items():
            instant = str(instant_int)
            # In wire value change command, the next value is the first symbol
            # and the rest of the symbols is the identifier code.
            vcd_value = f"{value}{identifier_code}"
            if instant in global_tv.keys():
                global_tv[instant].append(vcd_value)
            else:
                global_tv[instant] = [vcd_value]

        identifier_code += 1

    return (global_tv, var_declarations, smallest_time_unit)


def stringify_variable_declarations(declarations: Dict[str, Any]) -> str:
    """
    Transform variable declarations as a dictionary to VCD variable
    declarations.

    Example (indentations just for readability):
    $scope module toplvl $end
        $var wire 1 a clk $end
        $var wire 1 data_i $end
        $scope module my_gadget $end
            $var wire 1 data $end

    # ! FIXME
    # !
    # ! WHAT IF
    # ! scope 1
    # !     var 1_1
    # !     scope 2
    # !         var 2_1
    # !     scope 2 end
    # !     var 1_2
    # !     scope 3
    # !         var 3_1
    # !     scope 3 end
    # !     var 1_3
    # ! scope 1 end

    TODO
    """

    if not isinstance(declarations, dict):
        raise Exception(f"Declarations must be a dictionary.")

    result = StringIO()

    nodes: Deque[Dict[str, Any]] = Deque()
    nodes.append(declarations)

    # Add scope and variable declarations.
    scope_count = 0
    while 0 < len(nodes):
        node = nodes.pop()
        for child in node["children"].values():
            nodes.append(child)
        scope_name = node["name"]
        scope_kind = "module"  # ! how about other scope types?
        scope_count += 1
        print(f"hello from {scope_name}")
        if scope_name != "root":
            scope_declaration = f"$scope {scope_kind} {scope_name} $end\n"
            result.write(scope_declaration)
        for item in node["data"]:
            kind = "wire"  # ! how about other signal kinds?
            size = 1  # ! and sizes?
            identifier = item["identifier_code"]
            reference = item["reference"]
            variable_declaration = \
                f"$var {kind} {size} {identifier} {reference} $end\n"
            result.write(variable_declaration)

    # Ignore the root scope.
    scope_count -= 1

    # Add scope terminator declarations.
    # ! add these to nested scopes too...
    for _ in range(0, scope_count):
        result.write(f"$upscope $end\n")

    result.seek(0)

    return result.read()


def stringify_value_change_commands(
        time_value_pairs: Dict[str, List[str]]) -> str:
    """
    TODO
    """

    if not isinstance(time_value_pairs, dict):
        raise Exception(f"Time value pairs must be a dictionary.")

    result = StringIO()

    for instant, values in time_value_pairs.items():
        # Add the time change.
        time_change = f"#{instant}\n"
        result.write(time_change)
        # Add the value changes.
        for value in values:
            value_change = f"{value}\n"
            result.write(value_change)

    result.seek(0)

    return result.read()

# -----------------------------------------------------------------------------
# The binarization function.
# -----------------------------------------------------------------------------


def execute_transformation(
        targets: Dict[str, Dict[str, Any]],
        show_progress_bars: bool) -> StringIO:
    """
    Transform strings with signal data to value change dump (VCD) format.

    Parameters
    ----------
    targets: Dict[str, Dict[str, Any]]
        The outer dictionary key is the target name with the hierarchy path and
        the inner dictionary key is the property name and the dictionary value
        is the property value.
    show_progress_bars: bool
        Print the progress bars to the console.

    Returns
    -------
    StringIO
        The VCD file as a StringIO object.
    """

    if not isinstance(targets, dict):
        raise Exception(f"Targets must be a dictionary.")
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"Show progress bars must be a boolean.")

    result = StringIO()

    if not targets:
        return result

    # Get time value pairs, variable declarations and the smallest time unit.
    global_tv, var_declarations, smallest_time_unit = build_time_value_pairs(
        targets
    )

    # ! why not use \t ???
    time_scale_declaration = f"$timescale\n    1{smallest_time_unit}\n$end\n"
    result.write(time_scale_declaration)

    # Create and write scope and variable declarations.
    variable_declarations = stringify_variable_declarations(var_declarations)
    result.write(variable_declarations)

    # Create and write value change commands.
    value_change_commands = stringify_value_change_commands(global_tv)
    result.write("$enddefinitions $end\n")
    result.write(value_change_commands)

    return result

# -----------------------------------------------------------------------------
# File functions.
# -----------------------------------------------------------------------------


pattern = re.compile(r"(?P<identifier_expr>((?P<path>[\w_.]+(?=\.)).){0,1}(?P<identifier>[\w_]+) {0,}((?P<bracket>\[\d+(:{0,2}\d+|\d+){0,1}\]){0,1}){1}) {0,}(?P<time_scale_expr>(?P<number>\d{1,}) {0,}(?P<unit>[fpnum]{0,1}s{1})|\?){1} {1}(?P<offset_expr>(?P<offsetnumber>\d{1,}) {0,}(?P<offsetunit>[fpnum]{0,1}s{1})|\?){0,1} {0,1}(?P<string>[01xXzZ;]+)\Z")


def parse_input_stream(stream: TextIO) -> Dict:
    """
    Transform a binary string and knowledge about it to a dictionary object.

    Input stream syntax in BNF:

    stream         ::= name_with_path clock_period offset binary_strings
    name_with_path ::= (path_part '.')* name
    clock_period   ::= number unit
    offset         ::= number unit
    unit           ::= 's' | 'ms' | 'us' | 'ns' | 'ps' | 'fs'
    binary_strings ::= binary_string | binary_string ';' binary_strings
    binary_string  ::= binary_digit binary_digit*
    binary_digit   ::= '0' | '1'

    Parameters
    ----------
    TODO

    Returns
    -------
    TODO

    Raises
    ------
    TODO
    """

    if not isinstance(stream, (StringIO, TextIOWrapper)):
        raise Exception(f"Stream must be a StringIO or TextIOWrapper object.")

    from vcdstringtools import process_bracket_expression as pbe

    result = {}

    while True:
        line = stream.readline()

        if line == "":
            # End of file.
            break

        # Remove surrounding white space.
        line = line.strip()

        # Exclude empty lines.
        if line == "":
            continue

        # Exclude lines that begin with a number sign.
        if line[0] == "#":
            continue

        matches = pattern.search(line)

        if not matches:
            raise Exception(f"Could not parse line {line}")

        path = matches.group("path")
        identifier = matches.group("identifier")
        bracket = matches.group("bracket")
        number = int(matches.group("number"))
        unit_str = matches.group("unit")
        offsetnumber_obj = matches.group("offsetnumber")
        offsetunit_str = matches.group("offsetunit")
        string = matches.group("string")

        if "[" in line or "]" in line or ":" in line:
            if not bracket:
                raise Exception(f"Invalid bracket expression.")

        unit = TimeScaleUnit.from_string(unit_str)

        clock_period = TimeScale(number, unit)

        offsetnumber = None
        offsetunit = None
        offset = None
        if offsetnumber_obj and offsetunit_str:
            offsetnumber = int(offsetnumber_obj)
            offsetunit = TimeScaleUnit.from_string(offsetunit_str)
            offset = TimeScale(offsetnumber, offsetunit)

        name = identifier
        if path:
            name = ".".join([path, name])

        if bracket:
            bracket_info = pbe(bracket)
            if bracket_info.is_single_signal:
                index = bracket_info.signal_index
                full_name = "".join([name, "[", str(index), "]"])
                result[full_name] = {
                    "clock_period": clock_period,
                    "string": string,
                    "offset": offset
                }
            elif bracket_info.is_bus:
                indices = bracket_info.signal_index_range
                for index in indices:
                    full_name = "".join([name, "[", str(index), "]"])
                    result[full_name] = {
                        "clock_period": clock_period,
                        "string": string,
                        "offset": offset
                    }
            elif bracket_info.is_double_colon_expression:
                indices = bracket_info.signal_index_range
                for index in indices:
                    full_name = "".join([name, "[", str(index), "]"])
                    result[full_name] = {
                        "clock_period": clock_period,
                        "string": string,
                        "offset": offset
                    }
        else:
            result[name] = {
                "clock_period": clock_period,
                "string": string,
                "offset": offset
            }

    return result

# =============================================================================
# Script execution interface.
# =============================================================================


if __name__ == "__main__":
    from vcdutilities import check_user_python_version
    check_user_python_version()

    import argparse

    cmdparser = argparse.ArgumentParser(
        prog=f"bin2vcd",
        usage=None,
        description=f"TODO",
        epilog=f"TODO",
        parents=[],
        formatter_class=argparse.HelpFormatter,
        prefix_chars="-",
        fromfile_prefix_chars=None,
        argument_default=None,
        conflict_handler="error",
        add_help=True,
        allow_abbrev=True,
        exit_on_error=True
    )

    # --------------------
    # Mandatory arguments.
    # --------------------

    cmdparser.add_argument(
        "input",
        type=pathlib.Path,
        help="Full path to the file with binary strings."
    )
    cmdparser.add_argument(
        "output",
        type=pathlib.Path,
        help="Full path to the file where the VCD is saved to."
    )

    # -----------------------
    # User interface options.
    # -----------------------

    cmdparser.add_argument(
        "-wptc", "--write-progress-to-console",
        action="store_true",
        help="Let the script to report it's progress to the console."
    )
    cmdparser.add_argument(
        "-spb", "--show-progress-bars",
        action="store_true",
        help="Let the script to show it's parsing progress with progress bar."
    )

    args = vars(cmdparser.parse_args())

    # ------------------------------
    # Mandatory arguments unpacking.
    # ------------------------------

    input_file = args.get("input", None)
    if not isinstance(input_file, pathlib.Path):
        raise Exception(f"The input file must be a file.")
    if not input_file.exists():
        raise Exception(f"The input file must exist.")
    if not input_file.is_file():
        raise Exception(f"The input file must be a file.")
    if input_file.suffix != ".txt":
        raise Exception(f"The input file extension must be .txt.")

    output_file = args.get("output", None)
    if not isinstance(output_file, pathlib.Path):
        raise Exception(f"The output must be a file.")
    if not output_file.exists():
        raise Exception(f"The output file must exist.")
    if not output_file.is_file():
        raise Exception(f"The output file must be a file.")
    if output_file.suffix != ".vcd":
        raise Exception(f"The output file extension must be .vcd.")

    # ---------------------------------
    # User interface options unpacking.
    # ---------------------------------

    write_progress_to_console = args.get("write_progress_to_console", False)
    if not isinstance(write_progress_to_console, bool):
        raise Exception(f"The write progress to console option must be a "
                        "boolean.")

    if write_progress_to_console:
        # The user wants to read scipt progress reports.
        logging.basicConfig(level=logging.INFO)
    else:
        # Report only if something goes wrong.
        logging.basicConfig(level=logging.WARNING)

    show_progress_bars = args.get("show_progress_bars", False)
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"The show progress bars option must be a boolean.")

    # -------------------------
    # The transformation phase.
    # -------------------------

    logging.info(f"Reading the input file.")
    with input_file.open("r") as input_stream:
        targets = parse_input_stream(input_stream)
    logging.info(f"Input file reading finished.")

    logging.info(f"Transforming the strings to VCD.")
    transformation_args = (targets, show_progress_bars)
    vcd = execute_transformation(*transformation_args)
    logging.info(f"Transformation finished.")

    logging.info(f"Saving the VCD to output file.")
    with output_file.open("w") as output_stream:
        output_stream.write(vcd.getvalue())
    logging.info(f"Saving finished.")

    logging.info(f"Program finished.")
