"""
# TODO: update documentation

A VCD object is a Python data structure representation of a VCD file.
"""

# =============================================================================
# Standard library imports
# =============================================================================

from enum import Enum
from typing import Dict, List, Optional, Set, Union
from dataclasses import dataclass

# =============================================================================
# Local module imports
# =============================================================================

from vcdbus import Bus
from vcdsignal import Signal
from vcdtimescale import TimeScale
from vcdtimescaleunit import TimeScaleUnit

# =============================================================================
# ScopeType enumeration definition
# =============================================================================


class ScopeType(Enum):
    # The top level module and module instances.
    MODULE = "module"
    # Task.
    TASK = "task"
    # Function.
    FUNCTION = "function"
    # Named sequential block.
    BEGIN = "begin"
    # Named parallel block.
    FORK = "fork"

    def __repr__(self) -> str:
        return self.value

# =============================================================================
# Declaration object definitions
# ! missing docstrings
# =============================================================================


@dataclass
class DateDeclaration:
    """
    A container for the date on which the VCD file was generated.

    Attributes
    ----------
    date : str
        The date on which the VCD file was generated.
    """
    date: str

    def __repr__(self) -> str:
        return self.date


@dataclass
class VersionDeclaration:
    """
    A container for the version of the VCD writer which was used to produce the
    VCD file and the $dumpfile system task used to create the file.

    Attributes
    ----------
    version : str
        The version of the VCD writer which was used to produce the VCD file.

    system_task : str
        The $dumpfile system task used to create the file.
    """
    version: str
    system_task: str

    def __repr__(self) -> str:
        return f"{self.version} {self.system_task}"


@dataclass
class CommentDeclaration:
    """
    A container for the simulation comment.

    Attributes
    ----------
    comment : str
        The simulation comment.
    """
    comment: str

    def __repr__(self) -> str:
        return f"{self.comment}"


@dataclass
class ScopeDeclaration:
    """
    A container for a scope declaration.

    Attributes
    ----------
    kind : str
        The type of the scope.

    identifier : str
        The name of the scope.
    """
    kind: ScopeType
    identifier: str

    def __repr__(self) -> str:
        return f"{self.kind} {self.identifier}"


@dataclass
class VariableReference:
    """TODO"""
    name: str
    bit_select_index: int
    msb_index: int
    lsb_index: int

    def __repr__(self) -> str:
        return f"{self.name}"


# =============================================================================
# VCDObject definition
# =============================================================================


class VCDObject:
    """
    A VCD file represented as a Python data structure.

    Attributes
    ----------
    file_name : str
        The name of the file which is represented by this object.

    objects : Set[Union[Signal, Bus]]
        A set of all signals and buses.

    objects_list : List[Union[Signal, Bus]]
        A ordered list of all signals and buses.

    signals : Set[Signal]
        A set of all signals.

    signals_list : List[Signal]
        A ordered list of all signals.

    buses : Set[Bus]
        A set of all buses.

    buses_list : Set[Bus]
        A ordered list of all buses.

    objects_by_identifiers : Dict[str, Union[Signal, Bus]]
        A dictionary where the dictionary key is the VCD file identifier and
        the dictionary value is the object associated with the identifier.

    objects_by_scopes : Dict[str, Union[Signal, Bus]]
        A dictionary where the dictionary key is the scope name and the
        dictionary value is the object associated with the scope.

    simulation_date : Optional[DateDeclaration]
        The date when the simulation was executed.

    simulation_version : Optional[VersionDeclaration]
        The simulator version.

    simulation_comment : Optional[CommentDeclaration]
        The simulation comment.

    simulation_time_scale : Optional[TimeScale]
        The simulation time scale.

    simulation_skipped_time : TimeScale
        The amount of time skipped before the parsing.

    variables_initialized : Optional[TimeScale]
        When the variables were initialized.
    """

    # To save memory, to get faster attribute accesses and to deny the creation
    # of new attributes dynamically.
    __slots__ = [
        "file_name",
        "objects",
        "objects_list",
        "signals",
        "signals_list",
        "buses",
        "buses_list",
        "objects_by_identifiers",
        "objects_by_scopes",
        "simulation_date",
        "simulation_version",
        "simulation_comment",
        "simulation_time_scale",
        "simulation_skipped_time",
        "variables_initialized"
    ]

    def __init__(self) -> None:
        self.file_name: str = str()

        self.objects: Set[Union[Signal, Bus]] = set()
        self.objects_list: List[Union[Signal, Bus]] = list()

        self.signals: Set[Signal] = set()
        self.signals_list: List[Signal] = list()
        self.buses: Set[Bus] = set()
        self.buses_list: List[Bus] = list()

        self.objects_by_identifiers: Dict[str, Union[Signal, Bus]] = dict()
        self.objects_by_scopes: Dict[str, Set[Union[Signal, Bus]]] = dict()

        self.simulation_date: Optional[DateDeclaration] = None
        self.simulation_version: Optional[VersionDeclaration] = None
        self.simulation_comment: Optional[CommentDeclaration] = None
        self.simulation_time_scale: Optional[TimeScale] = None

        self.simulation_skipped_time: TimeScale = \
            TimeScale(0, TimeScaleUnit.FEMTOSECOND)

        self.variables_initialized: Optional[TimeScale] = None

    def __repr__(self) -> str:
        if not self.file_name or self.file_name == "":
            return f"VCDObject from unknown file."
        else:
            return f"VCDObject from {self.file_name}"
