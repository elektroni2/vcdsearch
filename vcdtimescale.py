"""
A time scale is a combination of a scalar and a unit which defines a range of
time.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import re

from typing import Union

# =============================================================================
# Local module imports
# =============================================================================

from vcdtimescaleunit import TimeScaleUnit

# =============================================================================
# Exception definitions
# =============================================================================


class InvalidTimeScaleExpression(Exception):
    """
    Raised when a textual representation of a time scale has a invalid format.
    """

    def __init__(self) -> None:
        self.message = f"Time scale expression has a invalid format."
        super().__init__(self.message)

# =============================================================================
# TimeScale definition
# =============================================================================


class TimeScale:
    """
    A combination of a scalar and a unit which defines a range of time.

    Attributes
    ----------
    number : int
        The scalar value.

    unit : TimeScaleUnit
        The multiplier of the scalar value.
    """

    # To save memory, to get faster attribute accesses and to deny the creation
    # of new attributes dynamically.
    __slots__ = [
        "number",
        "unit"
    ]

    def __init__(self, number: Union[int, str], unit: TimeScaleUnit) -> None:
        self.number = int(number)
        self.unit = unit

    def __repr__(self) -> str:
        return f"{self.number} {repr(self.unit)}"

    def __eq__(self, other: "TimeScale") -> bool:
        if not isinstance(other, TimeScale):
            raise Exception(f"You must only compare a time scale object to "
                            "another time scale object.")
        return self.to_scalar() == other.to_scalar()

    def __lt__(self, other: "TimeScale") -> bool:
        if not isinstance(other, TimeScale):
            raise Exception(f"You must only compare a time scale object to "
                            "another time scale object.")
        return self.to_scalar() < other.to_scalar()

    def __le__(self, other: "TimeScale") -> bool:
        if not isinstance(other, TimeScale):
            raise Exception(f"You must only compare a time scale object to "
                            "another time scale object.")
        return self.to_scalar() <= other.to_scalar()

    @staticmethod
    def from_string(expression: str) -> "TimeScale":
        """
        Attempt to transform a textual representation of a time scale to a time
        scale object.

        Parameters
        ----------
        expression: str
            The textual representation of a time scale.

        Returns
        -------
        TimeScale
            The time scale as a object.

        Raises
        ------
        InvalidTimeScale
            Could not transform the textual representation to a object.
        """

        # Matches for example "75ns", "1000 ps", "1234 s".
        pattern = r"\A([0-9]{1,}) {0,1}([fpnum]{0,1}s{1})\Z"
        results = re.search(pattern, expression)
        if not results:
            raise InvalidTimeScaleExpression()

        value_str, unit_str = results.groups()

        value = int(value_str)
        unit = TimeScaleUnit.from_string(unit_str)

        return TimeScale(value, unit)

    @staticmethod
    def zero() -> "TimeScale":
        """
        Create a new time scale object with value of zero.

        Returns
        -------
        TimeScale
            A time scale object with a value of zero.
        """

        return TimeScale.from_string("0fs")

    def to_scalar(self) -> float:
        """
        Transform a time scale object to a scalar number.

        Returns
        -------
        float
            The time scale as a floating point scalar.
        """

        return self.unit.to_scalar() * self.number
