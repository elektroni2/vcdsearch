# VCDSearch

THIS TOOL IS A WORK IN PROGRESS

Verify without touching the testbench files.

VCDSearch is a tool to query which signals contain a given binary sequence in a [value change dump (VCD)](https://en.wikipedia.org/wiki/Value_change_dump) file.

Warning! It is not guaranteed that this file is up to date or a complete reference of this project.

## Example Use Cases

This list of example use cases is not a complete list of possible use cases.

Assumptions:

* A1: A system is an object written in a hardware description language (HDL) and simulated with a HDL simulator.
* A2: The system contains objects called signals.
* A3: Each signal is associated with a value. This value changes as a function of time and the state of the system.
* A4: The state of the system is the combination of all the signals and their values at a given point of time.

### 1. Is A Given Value Present In The System?

You want to know if a specific value (or values) appear in the system.

This diagram is not a normative or a complete reference of the behavior of the system.
It uses the Markdown [Mermaid addon](https://mermaid-js.github.io/mermaid/#/).

```mermaid
sequenceDiagram
participant User
participant VCDSearch

User->>VCDSearch: Does a value X appear in the system?
alt was found
    VCDSearch->>User: Yes, a value X was detected at signals Y0...Yn at times t00,t01,...,t10,t11,...,tn0,tn1,...,tnm.
else was not found
    VCDSearch->>User: No, a value X was never detected.
end
```

*How is this useful?*
Let's say that you want to verify that a module works.
You feed it a known value X and expect that the output is Y.
With VCDSearch you can verify that the expected output appears at the correct signal at the correct time.

*But you can already check that with a testbench?*
That is true, but VCDSearch enables you to perform this check without modifying the testbench file.
This is especially useful if you run a program in the system and want to check if it creates expected values in the system.
Also, what if the hardware is changed.
Does the marker values still appear in the system as expected with the program?
With VCDSearch you can easily verify it by querying the marker value from the VCD file.

### 2. Does A Given Value Move In The System As Expected?

You want to know if a specific value appears and moves in the system as expected.

*How is this useful?*
Let's say that you have a collection of modules and you want to verify that their operands and outputs are as expected.
You feed a known marker value to the system and follow the movement and evolution of the value as it moves through the modules.
With VCDSearch you can verify that the expected values are present at correct signals at correct times.

## Dependencies

* The script is written and tested with [Python](https://www.python.org/) version 3.9.5.
* Progress bars are implemented with the [tqdm](https://github.com/tqdm/tqdm) module.

## Overall Project Status

This project is currently *a work in progress*.

These lists are not guaranteed to be up to date or complete.

### What Works

* Feed commands via the command line.
* Transform a VCD file to a Python data structure.
* Estimate the clock periods of signals and buses.
* Create binary sequences from signals and buses.
* Write a binarization report to .json and to .xml file.
* Write a search report to .json and to .xml file.
* Write a performance report to .txt file.

### What Does Not Work

* Binarize simple and complex signals and buses.
* Compare two VCD files.
* Automatically run a Modelsim simulation, create a WLF file, convert it to a VCD file and feed it to the script.

### Miscellaneous

* Parsing performance is surprisingly good.
The first strategy was to parse the VCD file line-by-line.
This strategy was very slow (over 20 minutes with 153 MB file with my computer).
The next strategy was to load the file to RAM and to parse it line-by-line.
This was a bit faster (over 5 minutes with a 153 MB file with my computer).
The current strategy is to use Python regular expressions [re](https://docs.python.org/3/library/re.html?highlight=regex) with memory mapped file [mmap](https://docs.python.org/3/library/mmap.html).
This is currently the fastest strategy (under 5 minutes with a 153 MB file with my computer.)
* Script structure is complex.
* Many files and functions lack proper documentation.
* Variable and function names are somewhat descriptive.
Overall readability is decent, but there are many undocumented sections.
Regular expressions are complex (suggestion: use [regex101](https://regex101.com/) to debug regular expressions).
* Many exceptions are still handled properly.
* It is unknown if the script is compatible with Python versions lower than 3.9.5.
* There are no clear directions about how to use the script.

## Signals And Buses

The VCD object stores signals and buses as separate objects.
A single bus can also contain multiple signals.

This diagram is not a normative or a complete reference of the behavior of the system.
It uses the Markdown [Mermaid addon](https://mermaid-js.github.io/mermaid/#/).

```mermaid
flowchart LR
VCDObject --> Signals
Signals --> Signal_0
Signals --> Signal_1
Signals --> Signal_...
Signals --> Signal_n
VCDObject --> Buses
Buses --> Bus_0
Buses --> Bus_1
Buses --> Bus_...
Buses --> Bus_m
Bus_0 --> Signal_00
Bus_0 --> Signal_01
Bus_0 --> Signal_0...
Bus_0 --> Signal_0k
```

### What Is A Signal?

A signal is a named collection of time value pairs (aka "tv pairs").
A time value pair is a tuple which contains a time value and a signal value.
A time value is a single point in time.
A signal value is the value of the signal at the given point in time.

### What Is A Bus?

A bus is a named collection of signals.
The signals' names are equal to the name of the containing bus, except with a bracketed index value which is used to identify unique signals.

## Sequence Diagram

This diagram attempts to visualize a single use case of the program as a sequence diagram.

This diagram is not a normative or a complete reference of the behavior of the system.
It uses the Markdown [Mermaid addon](https://mermaid-js.github.io/mermaid/#/).

```mermaid
sequenceDiagram
    participant User
    participant Simulator
    participant vcdparser
    participant vcdbinarizer
    participant vcdsearcher
    
    User->>Simulator: HDL simulation task
    activate Simulator
    Simulator->>User: VCD file
    deactivate Simulator

    User ->> vcdparser: Arguments & VCD file
    activate vcdparser
    vcdparser ->> User: Pickled VCD object file
    deactivate vcdparser

    User ->> vcdbinarizer: Arguments & Pickled VCD object file
    activate vcdbinarizer
    vcdbinarizer ->> User: Pickled VCD object (binarized) file
    deactivate vcdbinarizer

    User ->> vcdsearcher: Arguments & Pickled VCD object (binarized) file
    activate vcdsearcher
    vcdsearcher ->> User: Search report
    deactivate vcdsearcher
```

## Important Submodules

The parser, binarizer and searcher are callable via the command line.
They are decoupled by serializing the results and inputs with the Python standard library serializer [pickle](https://docs.python.org/3/library/pickle.html).

### vcdparser

This module attempts to transform a VCD file to a Python object which is then serialized.

This diagram is not a normative or a complete reference of the behavior of the system.
It uses the Markdown [Mermaid addon](https://mermaid-js.github.io/mermaid/#/).

```mermaid
sequenceDiagram
    participant User
    participant vcdparser
    participant pickle

    User->>vcdparser: VCD file
    activate vcdparser
    loop Parse
        vcdparser->>vcdparser: parse file content
    end
    vcdparser->>pickle: Python object
    activate pickle
    pickle->>vcdparser: Pickle object
    deactivate pickle
    vcdparser->>User: Pickle file
    deactivate vcdparser
```

### vcdbinarizer

This module is responsible for transforming the time value pairs to binary strings.

This diagram is not a normative or a complete reference of the behavior of the system.
It uses the Markdown [Mermaid addon](https://mermaid-js.github.io/mermaid/#/).

```mermaid
sequenceDiagram
    participant User
    participant vcdbinarizer
    participant pickle

    User->>vcdbinarizer: Pickle file
    activate vcdbinarizer
    vcdbinarizer->>pickle: Pickle object
    activate pickle
    pickle->>vcdbinarizer: Python object
    deactivate pickle
    loop Combine
        vcdbinarizer->>vcdbinarizer: combine separate signals
    end
    loop Separate
        vcdbinarizer->>vcdbinarizer: separate combined signals
    end
    loop Estimate clocks
        vcdbinarizer->>vcdbinarizer: (estimate clocks)
    end
    loop Estimate lengths
        vcdbinarizer->>vcdbinarizer: (estimate lengths)
    end
    loop Binarize
        vcdbinarizer->>vcdbinarizer: create binary sequences
    end
    vcdbinarizer->>pickle: Python object
    activate pickle
    pickle->>vcdbinarizer: Pickle object
    deactivate pickle
    vcdbinarizer->>User: Pickle file
    deactivate vcdbinarizer
```

Binarization consists of solving the signal offset, clock period, binary length and finally the binary sequence itself.

![alt text](binarization_timing_diagram0.png "Example timing diagram.")

In the upper image, from bottom to top, we see signal names on the left and their timing diagram on the right.
For example signal "signal0_10cp_0o" has a name "signal0", a clock period (cp) of 10 and an offset (o) of 0.

Let's assume that the signal data is collected beginning from the first rising edge of "reference_10cp".
We can then see that the signal "signal0_10cp_0o" has a value "high" right from the beginning.
Based on the available data, we can induce that it's offset is zero and clock period is 10 or lower.

Similarly, we can induce the clock periods and offsets for the rest of the signals.
Or can we?
At first the signal "signal4" seems to obey the same characteristics as the signal "signal0", but then it starts to have more unexpectable behavior.
We can immediatly see that it does not obey clock period of 10, but the shortest time between the value changes is 2,5.
We can then induce that the clock period must be 2,5 or lower.

Similarly we can estimate the clock period for signal "signal5".
One can say that it's clock period is equal or lower than 10.

Can you solve the clock period for signal "signal6"?
This is impossible without background information about it's clock period, because we only have two value changes, at t=5 and t=20.
You can of course argue that the clock period must be lower than 20-5=15, but this can give a large error, because the first time value might be just the initialization point, which means that the real clock period might be much more longer that 15.
This is especially bad if the data length is very long and the binarization sets very short clock period for the signal, so the produced binary sequence becomes massively long and massively wrong.

With background information about clock periods and offsets of course enables more accurate inductions.

#### Initialization Or Offset?

The first time point in the time value pairs can be either the signal's initialization point or the first rising/falling edge.

TODO

```mermaid
flowchart TD
A[Solve Offset] --> B(Is first t-value initialization or first cycle?);
B -- initialization --> C[Next t-value is real offset];
B -- first cycle --> D[Offset is first t-value];
```

### vcdsearcher

This module is responsible for searching the targets from the binary strings
time instants and time ranges.

This diagram is not a normative or a complete reference of the behavior of the
system.
It uses the Markdown [Mermaid addon](https://mermaid-js.github.io/mermaid/#/).

```mermaid
sequenceDiagram
    participant User
    participant vcdsearcher
    participant pickle

    User->>vcdsearcher: Pickle file
    activate vcdsearcher
    vcdsearcher->>pickle: Pickle object
    activate pickle
    pickle->>vcdsearcher: Python object
    deactivate pickle
    loop Time instants search
        vcdsearcher->>vcdsearcher: search for targets
    end
    loop Time ranges search
        vcdsearcher->>vcdsearcher: search for targets
    end
    vcdsearcher->>User: Search report
    deactivate vcdsearcher
```

## Other Submodules

TODO

## Contributing

Are you interested in contributing to this project?

Feel free to *make a pull-request* or *open an issue*.

## License

See [LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT) for details.
