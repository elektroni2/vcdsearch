"""
# TODO
"""

# =============================================================================
# Standard library imports
# =============================================================================

import re

from typing import Dict, Iterator, Optional, List, Tuple

# =============================================================================
# Local module imports
# =============================================================================

# =============================================================================
# Exception definitions
# =============================================================================

# =============================================================================
# Global variables
# =============================================================================

# match date declaration
pattern_date = r"(\$date)\s*(?P<date>.*)\s*(\$end)g"

# match declaration commands (everything before enddefinitions)
pattern_declarations = r"([\W\w\s]*)(?=(\$enddefinitions))"




# Matches the signal's or bus's name.
pattern_identifier = r"[\w]{1,}"

# Matches the signal's or bus's name with the hierarchy path included.
pattern_identifier_with_hierarchy = r"[\w.]+"

# Matches just the hierarchy path.
pattern_hierarchy_path = r"[\w.]+(?=\.)"
pattern_hierarchy_path_compiled = re.compile(pattern_hierarchy_path)

# Match the hierarchy path parts (needs global flag).
pattern_hierarchy_path_parts = r"((?P<PathPart>[\w]*)(?P<Separator>\.))"
pattern_hierarchy_path_parts_compiled = re.compile(pattern_hierarchy_path_parts)

def get_hierarchy_path_parts(string: str) -> List[str]:
    results = pattern_hierarchy_path_parts_compiled.findall(string)
    return results

# Matches just the bus description with brackets.
pattern_bus_description = r"\[\d+:{1}\d+\]"

# Match hierarchy path (if found), identifier name and bracket expression (if found).
pattern_full_identifier_expression = r"((?P<path>[\w_.]+(?=\.)).){0,1}(?P<identifier>[\w_]+) {0,}(?P<bracket>\[\d{1,}(:{0,2}\d{1,}){0,1}\]){0,1}"
pattern_full_identifier_expression_compiled = \
    re.compile(pattern_full_identifier_expression)

pattern_time_scale = r"(?P<number>\d{1,}) {0,}(?P<unit>[fpnum]{0,1}s{1})"
pattern_time_scale_compiled = re.compile(pattern_time_scale)


pattern_identifier_with_bus_description = r"([a-zA-Z0-9_.]+) {0,1}(\[\d+:{1}\d+\])"

# ---------------------
# For binarizer module.
# ---------------------

pattern_identifier_and_time_scale = r"(?P<identifier_expr>((?P<path>[\w_.]+(?=\.)).){0,1}(?P<identifier>[\w_]+) {0,}((?P<bracket>\[\d+(:{0,2}\d+|\d+){0,1}\]){0,1}){1}) {0,}(?P<time_scale_expr>(?P<number>\d{1,}) {0,}(?P<unit>[fpnum]{0,1}s{1})|\?){1}"


# =============================================================================
# Function definitions
# =============================================================================


pattern_signal_index = r"(?P<signal_index>(?<=\[)\d(?=\]))"
pattern_signal_index_compiled = re.compile(pattern_signal_index)


def get_signal_index(name: str) -> Optional[int]:
    result = pattern_signal_index_compiled.search(name)
    if result:
        return int(result.group("signal_index"))
    else:
        return None


def extract_hierarchy_path(expression: str) -> Optional[str]:
    result = pattern_hierarchy_path_compiled.search(expression)
    print(result)
    if result:
        return result.group(0)
    else:
        return None


class ProcessedBracketExpression:
    def __init__(self, **kwargs) -> None:
        self.is_single_signal = kwargs.get("is_single_signal", False)
        self.is_bus = kwargs.get("is_bus", False)
        self.is_double_colon_expression = kwargs.get("is_double_colon_expression", False)

        self.signal_index = kwargs.get("signal_index", None)
        self.signal_index_range = kwargs.get("signal_index_range", None)

    # -------------------------------------------------------------------------
    # Builder methods.
    # -------------------------------------------------------------------------

    @staticmethod
    def single_signal(index: int) -> "ProcessedBracketExpression":
        return ProcessedBracketExpression(is_single_signal=True, signal_index=index)

    @staticmethod
    def bus(signal_range: Iterator[int]) -> "ProcessedBracketExpression":
        return ProcessedBracketExpression(is_bus=True, signal_index_range=signal_range)

    @staticmethod
    def double_colon_expression(signal_range: Iterator[int]) -> "ProcessedBracketExpression":
        return ProcessedBracketExpression(is_double_colon_expression=True, signal_index_range=signal_range)


def process_bracket_expression(expression: str) -> ProcessedBracketExpression:
    expression = expression.strip("[]")
    if "::" in expression:
        parts = expression.split("::", maxsplit=1)
        ms_index_str, ls_index_str = parts
        ms_index = int(ms_index_str)
        ls_index = int(ls_index_str)
        signal_range = range(ls_index, ms_index + 1)
        return ProcessedBracketExpression.double_colon_expression(signal_range)
    elif ":" in expression:
        parts = expression.split(":", maxsplit=1)
        ms_index_str, ls_index_str = parts
        ms_index = int(ms_index_str)
        ls_index = int(ls_index_str)
        signal_range = range(ls_index, ms_index + 1)
        return ProcessedBracketExpression.bus(signal_range)
    else:
        signal_index = int(expression)
        return ProcessedBracketExpression.single_signal(signal_index)


class ProcessedIdentifierExpression:
    def __init__(self, path, path_parts, identifier, bracket_expression) -> None:
        self.path = path
        self.path_parts = path_parts
        self.identifier = identifier
        self.bracket_expression: Optional[ProcessedBracketExpression] = bracket_expression


def process_identifier_expression(string: str) -> ProcessedIdentifierExpression:
    matches = pattern_full_identifier_expression_compiled.match(string)
    path = matches.group("path")
    path_parts = path.split(".") if path else None
    identifier = matches.group("identifier")

    # The bracket expression.
    be = matches.group("bracket")

    # The processed bracket expression.
    pbe = process_bracket_expression(be) if be else None

    args = (path, path_parts, identifier, pbe)
    return ProcessedIdentifierExpression(*args)
