"""
A signal is a named group of time value pairs.
"""

# =============================================================================
# Standard library imports
# =============================================================================

from typing import Optional, OrderedDict, Dict
from collections import OrderedDict
from dataclasses import dataclass

# =============================================================================
# Local module imports
# =============================================================================

from vcdtiming import Timing
from vcdsignalkind import SignalKind

# =============================================================================
# SignalName definition
# =============================================================================


@dataclass
class SignalName:
    """
    A name of a signal.

    Attributes #! FIXME
    ----------
    with_bus_description : str
        The signal name with the bus description if the signal is part of a
        bus.

    without_bus_description : str
        The signal name without the bus description even if the signal is part
        of a bus.

    bus_description : str
        If the signal is part of a bus, the bus description is the index of the
        signal in the bus. Otherwise this value is None.
    """

    # To save memory, to get faster attribute accesses and to deny the creation
    # of new attributes dynamically.
    __slots__ = [
        "path",
        "without_bus_description",
        "bus_description"
    ]

    path: str
    without_bus_description: str
    bus_description: str

    def with_bus_description(self) -> str:
        if self.bus_description != "":
            return f"{self.without_bus_description}{self.bus_description}"
        else:
            return self.without_bus_description

    def with_path(self, without_bus_description=False) -> str:
        if without_bus_description:
            name = self.without_bus_description
        else:
            name = self.with_bus_description()
        if self.path != "":
            return f"{self.path}.{name}"
        else:
            return name

    def bus_description_as_int(self) -> Optional[int]:
        if self.bus_description:
            return int(self.bus_description.strip("[]"))
        else:
            return None

    def __repr__(self) -> str:
        return self.with_bus_description()

# =============================================================================
# Signal definition
# =============================================================================


class Signal:
    """
    A named group of time value pairs.

    Attributes
    ----------
    name : SignalName
        The name of the signal.

    kind : SignalKind
        The type of the signal tells what the signal represents or is part of.

    path : str
        The signal hierarchy path.

    tv : OrderedDict[int, str]
        The time value pairs of the signal tell at which time instants the
        signal value changes to given value. The dictionary key represents a
        time instant and the dictionary value represents a signal value change.

    timing : Timing
        Signal's binary sequence start time instant, clock period and the unit
        of time.

    binarization_failed : bool
        True if the binarization could not be performed.

    binary : str
        The time value pairs transformed to a binary sequence.
    """

    # To save memory, to get faster attribute accesses and to deny the creation
    # of new attributes dynamically.
    __slots__ = [
        "name",
        "kind",
        "tv",
        "timing",
        "binarization_failed",
        "binary"
    ]

    def __init__(
            self,
            name: SignalName,
            kind: SignalKind,
            **kwargs) -> None:
        self.name: SignalName = name
        self.kind: SignalKind = kind
        self.tv: OrderedDict[int, str] = kwargs.get("tv", OrderedDict())
        self.timing: Optional[Timing] = None
        self.binarization_failed: bool = False

        self.binary: Optional[str] = None

    def __repr__(self) -> str:
        return self.name.with_bus_description()

    # ? deprecated?
    def to_dictionary(self) -> Dict[str, Optional[str]]:
        data: Dict[str, Optional[str]] = {
            "name": self.name.with_bus_description(),
            "kind": str(self.kind),
            "binarization_failed": str(self.binarization_failed),
        }

        if not self.binarization_failed:
            data["binary"] = self.binary
            if self.timing:
                data["offset"] = \
                    f"{self.timing.offset} {self.timing.clock_period.unit}"
                data["clock_period"] = str(self.timing.clock_period)

        return data
