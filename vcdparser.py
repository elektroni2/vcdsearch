"""
Functions to parse a VCD file to a Python data structure.
"""

# ! ? implement safe mode and fast mode
# ! safe = all validations
# ! fast = no validations

# =============================================================================
# Standard library imports
# =============================================================================

import re
import logging

from io import StringIO, TextIOBase, TextIOWrapper
from enum import Enum
from typing import Dict, Set, TextIO, Deque, Tuple, List, Union, Optional
from pathlib import Path
from collections import deque

# =============================================================================
# Local module imports
# =============================================================================

from vcdbus import Bus, BusName
from vcdsignal import Signal, SignalName
from vcdtimescale import TimeScale
from vcdsignalkind import SignalKind
from vcdtimescaleunit import TimeScaleUnit

from vcdobject import \
    VCDObject, \
    DateDeclaration, \
    VersionDeclaration, \
    CommentDeclaration, \
    ScopeDeclaration, \
    VariableReference, \
    ScopeType

# =============================================================================
# Exception definitions
# =============================================================================


class UnknownExtension(Exception):
    """
    Raised when a value has unknown first symbol so its extension is unknown.
    """

    def __init__(self, value: str) -> None:
        self.message = f"No known extension for value {value}."
        super().__init__(self.message)


class ContextWasNotFinished(Exception):
    """
    Raised when the parser TODO
    """
    pass


class InvalidDeclaration(Exception):
    """
    Raised when a declaration command has a invalid structure.
    """
    pass


class InvalidVariableDeclaration(Exception):
    """
    Raised when a variable declaration has a invalid format.
    """
    def __init__(self, declaration: str) -> None:
        self.message = \
            f"Variable declaration {declaration} has a invalid format."
        super().__init__(self.message)


class InvalidValueChangeCommand(Exception):
    """
    Raised when a value change command has a invalid format.
    """

    def __init__(self, message: str) -> None:
        self.message = message
        super().__init__(self.message)

# =============================================================================
# Enumeration definitions
# =============================================================================


class SimulationContext(Enum):
    COMMENT = "$comment"
    DUMPALL = "$dumpall"
    DUMPOFF = "$dumpoff"
    DUMPON = "$dumpon"
    DUMPVARS = "$dumpvars"

    def __str__(self) -> str:
        return self.value

    def __repr__(self) -> str:
        return self.value

# =============================================================================
# ParserState definition
# =============================================================================


class ParserState:
    """
    The current state of the VCD file parser.

    Attributes
    ----------
    all_declarations_parsed : bool
        True if all the VCD file declarations have been parsed. After the
        declarations, there are only simulation commands remaining.

    scope_stack : Deque[ScopeDeclaration]
        The stack of the variable declaration scopes.

    scope_stack_path : Deque[str]
        The stack of the variable declaration scopes as strings.

    current_scope_path : str
        The path to the current variable declaration scope.

    time_stamp : int
        The current time instant.

    allowed_identifiers : Optional[Set[int]]
        The identifiers of signals and buses which are parsed. If everything is
        parsed, then None.

    vcd_object : VCDObject
        The VCD file as a Python object.
    """

    def __init__(self) -> None:
        self.all_declarations_parsed = False

        self.scope_stack: Deque[ScopeDeclaration] = deque()
        self.scope_stack_path: Deque[str] = deque()
        self.current_scope_path = None
        self.time_stamp: Optional[int] = None
        self.allowed_identifiers: Optional[Set[str]] = None

        self.vcd_object = VCDObject()

    def __repr__(self) -> str:
        return f"ParserState from {self.vcd_object}"


# =============================================================================
# Function definitions
# =============================================================================


def validate_targets(targets: List[str]) -> List[str]:
    """
    Check that the targets have correct formats and filter out white space and
    commented lines.

    Parameters
    ----------
    targets: List[str]
        The list of target object names.

    Returns
    -------
    List[str]
        The validated list of target object names.

    Raises
    ------
    TODO
    """

    from vcdutilities import remove_duplicates
    from vcdstringtools import process_identifier_expression

    result: List[str] = []

    for target in targets:
        # Remove the surrounding white space.
        target = target.strip()

        # Exclude empty names.
        if target == "":
            continue

        # Exclude out-commented names.
        if target[0] == "#":
            continue

        ie = process_identifier_expression(target)

        if ie.path:
            name = f"{ie.path}.{ie.identifier}"
        else:
            name = ie.identifier

        if ie.bracket_expression:
            if ie.bracket_expression.is_single_signal:
                # Just add the signal index.
                index = ie.bracket_expression.signal_index
                name = f"{name}[{index}]"
                result.append(name)
            elif ie.bracket_expression.is_bus:
                # Expand the bus expression.
                indices = ie.bracket_expression.signal_index_range
                for index in indices:
                    separated_name = f"{name}[{index}]"
                    result.append(separated_name)
            elif ie.bracket_expression.is_double_colon_expression:
                # Expand the double colon expression.
                indices = ie.bracket_expression.signal_index_range
                for index in indices:
                    separated_name = f"{name}[{index}]"
                    result.append(separated_name)
        else:
            result.append(name)

    # Remove duplicate names.
    result = remove_duplicates(result)

    return result

# -----------------------------------------------------------------------------
# Declaration command parsing functions
# -----------------------------------------------------------------------------


def construct_signal(
        kind: SignalKind,
        reference: VariableReference,
        path: str) -> Signal:
    """
    Create a signal object from the instructions from the variable declaration.

    Parameters
    ----------
    kind: SignalKind
        The type of the signal.
    reference: VariableReference
        TODO

    Returns
    -------
    Signal
        The signal object as instructed from the variable declaration.
    """

    # Construct the signal name.
    # bd = bus description.
    without_bd = reference.name

    bd = "" if reference.bit_select_index is None \
        else f"[{reference.bit_select_index}]"

    name = SignalName(path, without_bd, bd)

    return Signal(name, kind)


def construct_bus(
        kind: SignalKind,
        size: int,
        reference: VariableReference,
        path: str) -> Bus:
    """
    TODO
    """

    # Create the index range based on the indices or the size.
    if reference.msb_index is not None and \
            reference.lsb_index is not None:
        index_range = \
            (int(reference.msb_index), int(reference.lsb_index))
    else:
        index_range = (size - 1, 0)

    # Construct the bus name.
    # rd = range description.

    index_1 = reference.name.find("[")
    if index_1 != -1:
        # Range description is already in the name.
        without_rd = reference.name[:index_1]
        rd = reference.name[index_1:]

    else:
        # Create the range description based on the index range.
        without_rd = reference.name
        start, end = [str(index) for index in index_range]
        rd = f"[{start}:{end}]"

    name = BusName(path, without_rd, rd)

    return Bus(name, kind, size, index_range)


pattern_variable_declaration = re.compile(
    r"(?P<type>\S*) {1}(?P<size>\d*) {1}(?P<identifier>\S*) {1}(?P<reference>[a-zA-Z0-9_]*)( {0,1}\[(?P<bracket>(?P<msb>\d*)(:(?P<lsb>\d*)){0,1})\]){0,1}"
)


def parse_variable_declaration(
        declaration: str,
        path: str) -> Tuple[str, Union[Signal, Bus]]:
    """
    Validate and construct a variable according to the declaration.

    Parameters
    ----------
    declaration: str
        The variable declaration.
    path: str
        The variable declaration hierarchy path.

    Returns
    -------
    Tuple[str, Union[Signal, Bus]]
        The identifier code and the structure.

    Raises
    ------
    InvalidVariableDeclaration
        The variable declaration has a invalid format.
    NotImplementedError
        The variable declaration has properties that are not yet supported.
    """

    match = pattern_variable_declaration.search(declaration)
    var_type = match.group("type")
    var_size = match.group("size")
    identifier = match.group("identifier")
    reference = match.group("reference")
    var_msb = match.group("msb")
    var_lsb = match.group("lsb")

    # Validate type, size, and the reference.
    variable_kind = SignalKind(var_type)
    if variable_kind.value not in {"wire", "reg", "integer"}:
        raise NotImplementedError(
            f"Can not yet handle variable kind {variable_kind}")
    variable_size = int(var_size)
    if var_msb:
        if var_lsb:
            args = (reference, None, var_msb, var_lsb)
        else:
            # Most significant index is a bit select index.
            args = (reference, var_msb, None, None)
    else:
        args = (reference, None, None, None)
    variable_reference = VariableReference(*args)

    # Implement the structure according to the declaration instructions.
    if variable_kind in {SignalKind.REG, SignalKind.INTEGER}:
        args = (variable_kind, variable_size, variable_reference, path)
        structure = construct_bus(*args)

    elif variable_kind in {SignalKind.WIRE}:
        args = (variable_kind, variable_reference, path)
        structure = construct_signal(*args)

    else:
        raise NotImplementedError(
            f"Can not yet handle variable kind {variable_kind}")

    return (identifier, structure)


def parse_simulation_date_declaration(
        declaration: str) -> DateDeclaration:
    """
    Validate and construct a date object according to the declaration.

    Date declaration syntax in BNF:
    date_declaration ::= date

    Parameters
    ----------
    declaration: str
        The date declaration.

    Returns
    -------
    DateDeclaration
        The date object.
    """

    return DateDeclaration(declaration)


def parse_simulation_version_declaration(
        declaration: str) -> VersionDeclaration:
    """
    Validate and construct a version object according to the declaration.

    Version declaration syntax in BNF:
    version_declaration ::= version system_task

    Parameters
    ----------
    declaration: str
        The version declaration.

    Returns
    -------
    VersionDeclaration
        The version object.
    """

    # ! implement system task recognition

    return VersionDeclaration(declaration, None)


def parse_simulation_comment_declaration(
        declaration: str) -> CommentDeclaration:
    """
    Validate and construct a comment object according to the declaration.

    Comment declaration syntax in BNF:
    comment_declaration ::= comment

    Parameters
    ----------
    declaration: str
        The comment declaration.

    Returns
    -------
    CommentDeclaration
        The comment object.
    """

    return CommentDeclaration(declaration)


pattern_timescale_declaration = re.compile(
    r"(?P<number>1{1}0{0,2}) {0,1}(?P<unit>[fpnum]{0,1}s)"
)


def parse_simulation_time_scale_declaration(
        declaration: str) -> TimeScale:
    """
    Validate and construct a time scale object according to the declaration.

    Time scale declaration syntax in BNF:
    time_scale_declaration ::= time_number time_unit

    Parameters
    ----------
    declaration: str
        The time scale declaration.

    Returns
    -------
    TimeScale
        The time scale object.

    Raises
    ------
    InvalidDeclaration
        The time scale declaration has a invalid format.
    """

    match = pattern_timescale_declaration.search(declaration)
    number = int(match.group("number"))
    unit = TimeScaleUnit.from_string(match.group("unit"))

    return TimeScale(number, unit)


def parse_scope_declaration(declaration: str) -> ScopeDeclaration:
    """
    Validate and construct a scope object according to the declaration.

    Scope declaration syntax in BNF:
    scope_declaration ::= type identifier

    Parameters
    ----------
    declaration: str
        The scope declaration.

    Returns
    -------
    ScopeDeclaration
        The scope object.

    Raises
    ------
    InvalidDeclaration
        The scope declaration has a invalid format.
    """

    kind, identifier = declaration.split(" ", maxsplit=1)

    return ScopeDeclaration(ScopeType(kind), identifier)


def process_declaration(
        state: ParserState,
        targets: List[str],
        context_data: str) -> None:
    """
    TODO
    """

    # Extract the context identifier.
    context_parts: List[str] = []
    context_add = context_parts.append  # Avoid dot expression re-evaluation.
    for symbol in context_data:
        if len(context_parts) == 0:
            context_add(symbol)
        else:
            if symbol == " " or symbol == "\n":
                break
            else:
                context_add(symbol)

    context = "".join(context_parts).strip()

    # Remove the context identifier and surrounding white space.
    context_data = context_data[len(context):].strip()

    # Comparisons are ordered in their frequency.
    vcd_object = state.vcd_object  # Declutter the code a bit.
    if context == "var":
        args = (context_data, state.current_scope_path)
        identifier, structure = parse_variable_declaration(*args)

        if targets:
            if structure.name in targets:
                # This variable is a target.
                state.allowed_identifiers.add(identifier)
            else:
                # This variable is ignored.
                return

        # Add the reference to the structure to multiple containers.

        vcd_object.objects.add(structure)

        if isinstance(structure, Signal):
            vcd_object.signals.add(structure)
        elif isinstance(structure, Bus):
            vcd_object.buses.add(structure)

        vcd_object.objects_by_identifiers[identifier] = structure

        if state.current_scope_path in vcd_object.objects_by_scopes.keys():
            vcd_object.objects_by_scopes[state.current_scope_path]. \
                add(structure)
        else:
            vcd_object.objects_by_scopes[state.current_scope_path] = \
                {structure}

    elif context == "scope":
        scope_declaration = parse_scope_declaration(context_data)
        state.scope_stack.append(scope_declaration)
        state.scope_stack_path.append(scope_declaration.identifier)
        state.current_scope_path = ".".join(state.scope_stack_path)

    elif context == "upscope":
        state.scope_stack.pop()
        state.scope_stack_path.pop()
        state.current_scope_path = ".".join(state.scope_stack_path)

    elif context == "date":
        vcd_object.simulation_date = \
            parse_simulation_date_declaration(context_data)

    elif context == "version":
        vcd_object.simulation_version = \
            parse_simulation_version_declaration(context_data)

    elif context == "timescale":
        vcd_object.simulation_time_scale = \
            parse_simulation_time_scale_declaration(context_data)

    elif context == "comment":
        vcd_object.simulation_comment = \
            parse_simulation_comment_declaration(context_data)

    elif context == "enddefinitions":
        state.all_declarations_parsed = True

# -----------------------------------------------------------------------------
# Simulation command parsing functions
# -----------------------------------------------------------------------------


EXTENSION_MAP = {
    "0": "0",
    "1": "0",
    "x": "x",
    "X": "x",
    "z": "z",
    "Z": "z"}


# ! HOT CODE
def expand_value(value: str, width: int) -> str:
    """
    Expand a compressed value.

    Parameters
    ----------
    value: str
        The value to be extended.
    width: int
        The uncompressed width of the value.

    Returns
    -------
    std
        The uncompressed value.

    Raises
    ------
    UnknownExtension
        The value has unknown first symbol so its extension symbol is unknown.

    Examples
    --------
        4 bit bus value 10 transforms to 0010 with extension 0

        4 bit bus value x1 transforms to xxx1 with extension x
    """

    try:
        return value.rjust(width, EXTENSION_MAP[value[0]])
    except KeyError:
        raise UnknownExtension(value)


# ! HOT CODE
# ! CODE CHANGED
def parse_value_change_command(
        objects_by_identifiers: Dict[str, Union[Signal, Bus]],
        time_stamp,
        allowed_identifiers: Optional[Set[str]],
        command: str) -> None:
    """
    TODO

    Value change command syntax in BNF:

    value_change_command ::= scalar_value_change
                           | vector_value_change

    scalar_value_change ::= value identifier_code

    vector_value_change ::= b binary_number identifier code
                          | B binary_number identifier_code
                          | r real_number identifier_code
                          | R real_number identifier_code

    Parameters
    ----------
    state: ParserState
        The current state of the VCD parser.
    command: str
        The value change command to be processed.

    Raises
    ------
    InvalidValueChangeCommand

    NotImplementedError
        Encountered a command that is not yet supported.
    """

    parts = command.split(" ")
    length = len(parts)

    if length == 1:
        # Variable is a scalar.
        #value = command[0]
        #identifier = command[1:]

        #state. \
        #    vcd_object. \
        #    objects_by_identifiers[identifier]. \
        #    tv[state.time_stamp] = value

        #state. \
        #    vcd_object. \
        #    objects_by_identifiers[command[1:]]. \
        #    tv[state.time_stamp] = command[0]

        if allowed_identifiers:
            if command[1:] not in allowed_identifiers:
                return

        objects_by_identifiers[command[1:]].tv[time_stamp] = command[0]

    elif length == 2:
        # Variable is a vector.
        value, identifier = parts

        if allowed_identifiers:
            if identifier not in allowed_identifiers:
                return

        # Remove the base letter.
        value = value[1:]

        # Validate the value.

        # ! use me? (1/2)
        #for symbol in value:
        #    if symbol not in SCALAR_DIGITS:
        #        raise InvalidValueChangeCommand(f"Value change command value
        # {value} symbol {symbol} is illegal.")  # ? why?

        # ! or me? (2/2)
        for symbol in value:
            if symbol != "0":
                if symbol != "1":
                    if symbol != "z":
                        if symbol != "x":
                            if symbol != "Z":
                                if symbol != "X":
                                    raise InvalidValueChangeCommand(
                                        f"Value change command value {value} "
                                        "symbol {symbol} is illegal.")

        objects_by_identifiers[identifier].tv[time_stamp] = expand_value(
            value,
            int(objects_by_identifiers[identifier].size)
        )

        #state. \
        #    vcd_object. \
        #    objects_by_identifiers[identifier]. \
        #    tv[state.time_stamp] = \
        #    expand_value(
        #        value,
        #        int(state.vcd_object.objects_by_identifiers[identifier].size))

    else:
        # ! events and maybe other things?
        raise NotImplementedError(
            f"Can not yet handle value change command {command}")

# -----------------------------------------------------------------------------
# Executor function
# ! TODO: choose parsing strategy with byte count as a decision variable
# -----------------------------------------------------------------------------


# ! memory hog, collects all lines to RAM to speed up the parsing
def parse_vcd2(
        file_stream: TextIO,
        targets: List[str],
        show_progress_bars: bool) -> ParserState:
    """
    Transform a VCD file to a Python data structure.

    Parameters
    ----------
    file_stream: TextIO
        A text stream object to the VCD file.
    targets: List[str]
        A list of target signal and bus names. Other signals and buses are not
        parsed. If empty, all signals and buses are parsed.
    show_progress_bars: bool
        True if progress bars are used and printed to the console.

    Returns
    -------
    ParserState
        The state of the parser after the parsing.

    Raises
    ------
    TODO
    """

    if not isinstance(file_stream, TextIOBase):
        raise Exception(f"The VCD file stream must be a text stream object.")
    if not isinstance(targets, list):
        raise Exception(f"The targets list must be a list.")
    if not isinstance(show_progress_bars, bool):
        raise Exception(f"The show progress bars option must be a boolean.")

    from typing import ContextManager

    # The progress bar wrapper for the value change command processing.
    def progress_bar(total: int) -> ContextManager:
        from vcdutilities import get_progress_bar_wrapper
        pbw = get_progress_bar_wrapper(show_progress_bars)
        return pbw([], total=total, desc="Value change commands parsed")

    file_stream_read = file_stream.read

    state = ParserState()

    if targets:
        state.allowed_identifiers = set()

    time_stamp = state.time_stamp
    objects_by_identifiers = state.vcd_object.objects_by_identifiers
    allowed_identifiers = state.allowed_identifiers

    context_data: Optional[Union[List[str], str]] = None

    # ---------------------------
    # Parse declaration commands.
    # ---------------------------
    while not state.all_declarations_parsed:
        byte = file_stream_read(1)

        if byte == "":
            # End of the file.
            break

        if context_data is None:
            if byte == "$":
                context_data = []
                context_data_append = context_data.append

                maybe_end_marker: Deque[str] = deque(maxlen=4)
                maybe_end_marker_append = maybe_end_marker.append

                # Read bytes until a $end marker has been reached.
                while True:
                    byte = file_stream_read(1)

                    if byte == "":
                        raise Exception(
                            f"Reached the end of the file before a "
                            "closing $end marker.")

                    context_data_append(byte)
                    maybe_end_marker_append(byte)

                    # ! join performance kill?
                    if "".join(maybe_end_marker) == "$end":
                        # Remove the $end marker, join the rest and
                        # remove the white space.
                        context_data = (
                            "".join(context_data[:len(context_data) - 4])
                        ).strip()
                        process_declaration(state, targets, context_data)
                        context_data = None

                        if state.all_declarations_parsed:
                            # Remove the newline.
                            _ = file_stream_read(1)

                        break
            else:
                # Ignore until a new context begins.
                pass
        else:
            raise ContextWasNotFinished()

    # --------------------------
    # Parse simulation commands.
    # ! handle comments, dumpon, dumpoff, dumpvars, dumpall
    # --------------------------

    # ! ? What if the file is too large to be fitted to memory?
    lines = deque(file_stream.readlines())
    lines_popleft = lines.popleft
    line = ""
    with progress_bar(len(lines)) as bar:
        bar_update = bar.update
        while 0 < len(lines):
            line = lines_popleft()
            bar_update(1)

            if line == "\n":
                continue

            if line[0] == "#":
                # Parse time change command. Get the string after
                # the number sign, remove the trailing white space
                # and convert to a integer.
                time_stamp = int(line[1:].rstrip())

            # dumpoff -> pysäytä muuttujien tallennus VCD tiedostoon
            # -> oletetaan arvoiksi X
            # -> binaari useassa osassa?
            # dumpon -> aloita tallennus uudelleen
            # -> arvo on muuttujan arvo sillä hetkellä

            elif line[0] == "$":
                # Comment or dumpx. Can be a multiline...
                if line[0:8] == "$comment":
                    context = SimulationContext.COMMENT
                elif line[0:8] == "$dumpall":
                    # Specifies current values of all variables dumped.
                    context = SimulationContext.DUMPALL
                elif line[0:8] == "$dumpoff":
                    # Indicates all variables dumped with X values.
                    context = SimulationContext.DUMPOFF
                elif line[0:7] == "$dumpon":
                    # Indicates resumption of dumping and lists current values
                    # of all variables dumped.
                    context = SimulationContext.DUMPON
                elif line[0:9] == "$dumpvars":
                    # Lists initial values of all variables dumped.
                    context = SimulationContext.DUMPVARS
                else:
                    raise Exception(f"Unknown simulation command.")

                lines_to_end = Deque()
                lines_to_end.append(line)
                while 0 < len(lines):
                    line = lines_popleft()
                    bar_update(1)
                    lines_to_end.append(line)
                    if "$end" in line:
                        break

                context_data = ""
                for line in lines_to_end:
                    context_str = str(context)
                    if context_str in line:
                        line = line.replace(context_str, "")
                    if "$end" in line:
                        line = line.replace("$end", "")
                    context_data = f"{context_data} {line}"
                context_data = context_data.strip()

                if context is SimulationContext.COMMENT:
                    state.vcd_object.simulation_comment = \
                        parse_simulation_comment_declaration(context_data)

                elif context is SimulationContext.DUMPALL:
                    raise NotImplementedError()

                elif context is SimulationContext.DUMPOFF:
                    changes = Deque(context_data.split(" "))
                    while 0 < len(changes):
                        change = changes.popleft()
                        if change[0] in {"b", "B", "r", "R"}:
                            # Vector has two parts.
                            second_part = changes.popleft()
                            change = f"{change} {second_part}"
                        parse_value_change_command(
                            objects_by_identifiers,
                            time_stamp,
                            allowed_identifiers,
                            change
                        )

                elif context is SimulationContext.DUMPON:
                    raise NotImplementedError()

                elif context == SimulationContext.DUMPVARS:
                    changes = Deque(context_data.split(" "))
                    while 0 < len(changes):
                        change = changes.popleft()
                        if change[0] in {"b", "B", "r", "R"}:
                            # Vector has two parts.
                            second_part = changes.popleft()
                            change = f"{change} {second_part}"
                        parse_value_change_command(
                            objects_by_identifiers,
                            time_stamp,
                            allowed_identifiers,
                            change
                        )

            else:
                # Value change command.
                parse_value_change_command(
                    objects_by_identifiers,
                    time_stamp,
                    allowed_identifiers,
                    line.strip()
                )

    return state


class FileStreamNotTextIO(Exception):
    """
    Raised when a file stream is not a TextIO object.
    """
    def __init__(self) -> None:
        self.message = f"File stream must be a TextIOWrapper or a StringIO object."
        super().__init__(self.message)


class VCDFileStreamWrapper:
    def __init__(self, file_stream: TextIO) -> None:
        self.stream = file_stream
        self.mapping = None

        if isinstance(file_stream, TextIOWrapper):
            self.kind = "file"
        elif isinstance(file_stream, StringIO):
            self.kind = "not-file"
        else:
            raise Exception()

    def __enter__(self):
        if isinstance(self.stream, TextIOWrapper):
            # FIXME: mmap interface is different on windows and linux
            import platform
            import mmap
            args = None
            if platform.system() == "Linux":
                args = (self.stream.fileno(), 0, mmap.MAP_PRIVATE, mmap.ACCESS_READ)
            elif platform.system() == "Windows":
                args = (self.stream.fileno(), 0, None, mmap.ACCESS_READ)
            else:
                raise Exception(f"Platform not supported yet: {platform.system()}.")
            self.mapping = mmap.mmap(*args)
            return self.mapping
        elif isinstance(self.stream, StringIO):
            return str.encode(self.stream.read())

    def __exit__(self, type, value, traceback):
        if isinstance(self.stream, TextIOWrapper):
            self.mapping.close()
        else:
            pass


# ! regex with memory mapped file
def parse_vcd(
        file_stream: TextIO,
        targets: List[str],
        show_progress_bars: bool) -> ParserState:
    """TODO"""

    if not isinstance(file_stream, (TextIOWrapper, StringIO)):
        raise FileStreamNotTextIO()
    if not isinstance(targets, list):
        raise Exception()
    if not isinstance(show_progress_bars, bool):
        raise Exception()

    # Matches a time scale declaration.
    pat_time_scale = re.compile(
        rb"\$timescale(?P<data>.*?)\$end", re.MULTILINE | re.DOTALL
    )
    # Matches a version declaration.
    pat_version = re.compile(
        rb"\$version(?P<data>.*?)\$end", re.MULTILINE | re.DOTALL
    )
    # Matches a comment declaration.
    pat_comment = re.compile(
        rb"\$comment(?P<data>.*?)\$end", re.MULTILINE | re.DOTALL
    )
    # Matches a date declaration.
    pat_date = re.compile(
        rb"\$date(?P<data>.*?)\$end", re.MULTILINE | re.DOTALL
    )
    # Matches a end definitions declaration.
    pat_enddefinitions = re.compile(
        rb"\$enddefinitions(?P<data>.*?)\$end", re.MULTILINE | re.DOTALL
    )
    # Matches either a scope, variable or a upscope declaration.
    pat_scope_var_upscope = re.compile(
        rb"\$scope(?P<scope>.*?)\$end|\$var(?P<var>.*?)\$end|\$upscope(?P<upscope>.*?)\$end"
    )

    state = ParserState()

    # Wrap the stream to adapt for different types of streams.
    stream = VCDFileStreamWrapper(file_stream)

    # ! set better name for 'm'...
    with stream as m:
        # --------------------------
        # Find declaration commands.
        # --------------------------

        # Solve the byte index which sets the end of the declaration command
        # search space.
        end_definitions = pat_enddefinitions.search(m)
        if not end_definitions:
            raise Exception(f"Expected to find definitions end marker.")
        declarations_end = end_definitions.span()[0]
        simulation_start = end_definitions.span()[1]
        del end_definitions
        del pat_enddefinitions

        time_scale = pat_time_scale.search(m, endpos=declarations_end)
        if not time_scale:
            raise Exception("A VCD file must declare a time scale.")
        time_scale = time_scale.group("data")
        state.vcd_object.simulation_time_scale = \
            parse_simulation_time_scale_declaration(
                time_scale.decode("utf8").strip()
            )
        del time_scale
        del pat_time_scale

        maybe_version = pat_version.search(m, endpos=declarations_end)
        if maybe_version:
            version = maybe_version.group("data")
            state.vcd_object.simulation_version = \
                parse_simulation_version_declaration(
                    version.decode("utf8").strip()
                )
            del version
        del maybe_version
        del pat_version

        maybe_date = pat_date.search(m, endpos=declarations_end)
        if maybe_date:
            date = maybe_date.group("data") if maybe_date else None
            state.vcd_object.simulation_date = \
                parse_simulation_date_declaration(
                    date.decode("utf8").strip()
                )
            del date
        del maybe_date
        del pat_date

        # To avoid re-evaluating the dot expression.
        allowed_identifiers = state.allowed_identifiers
        obs = state.vcd_object.objects_by_scopes
        obi = state.vcd_object.objects_by_identifiers
        objects = state.vcd_object.objects
        objects_list = state.vcd_object.objects_list
        signals = state.vcd_object.signals
        signals_list = state.vcd_object.signals_list
        buses = state.vcd_object.buses
        buses_list = state.vcd_object.buses_list

        # Parse scope and variable declarations. A scope contains variables and
        # maybe nested scopes. A single scope ends with an upscope command.
        scopes = Deque()
        scope_path = Deque()
        scope_path_str = ""
        currpos = 0
        while True:
            # Get next declaration command from the search space.
            expr = pat_scope_var_upscope.search(
                m, pos=currpos, endpos=declarations_end
            )
            if not expr:
                break

            # Move the search space limiter forward.
            currpos = expr.span()[1]

            if expr.lastgroup == "scope":
                # Create a new variable scope.
                decl = parse_scope_declaration(
                    expr.group("scope").decode("utf8").strip()
                )
                scopes.append(decl)
                scope_path.append(decl.identifier)
                scope_path_str = ".".join(scope_path)

            elif expr.lastgroup == "var":
                # Create a new variable to the current scope.
                context_data = expr.group("var").decode("utf8").strip()
                args = (context_data, scope_path_str)
                identifier, structure = parse_variable_declaration(*args)
                allow_variable = True
                if targets:
                    if structure.name.with_path() in targets:
                        # This variable is a target.
                        if allowed_identifiers is None:
                            allowed_identifiers = set()
                        allowed_identifiers.add(identifier)
                    else:
                        # This variable is ignored.
                        allow_variable = False
                if allow_variable:
                    # Add the new variable object to various mappings.
                    objects.add(structure)
                    objects_list.append(structure)
                    if isinstance(structure, Signal):
                        signals.add(structure)
                        signals_list.append(structure)
                    elif isinstance(structure, Bus):
                        buses.add(structure)
                        buses_list.append(structure)
                    obi[identifier] = structure
                    if scope_path_str in obs.keys():
                        obs[scope_path_str].add(structure)
                    else:
                        obs[scope_path_str] = {structure}

            elif expr.lastgroup == "upscope":
                scopes.pop()
                scope_path.pop()
                scope_path_str = ".".join(scope_path)
        del scopes
        del scope_path
        del scope_path_str
        del currpos
        del pat_scope_var_upscope

        if targets:
            assert allowed_identifiers is not None

        # A comment can exists everywhere.
        maybe_comment = pat_comment.search(m)
        comment_span = None
        if maybe_comment:
            comment = maybe_comment.group("data") if maybe_comment else None
            state.vcd_object.simulation_comment = \
                parse_simulation_comment_declaration(
                    comment.decode("utf8").strip()
                )
            comment_span = maybe_comment.span()
        del maybe_comment
        del pat_comment

        # ----------------------------
        # Process simulation commands.
        # ----------------------------

        # Matches either a time stamp, scalar value change or a vector value
        # change command.
        pat_vcc = re.compile(
            rb"#(?P<timestamp>\d*)\s*|(?<=\s)(?P<scalar>[\dxXzZ]{1}\S*)|(?P<vector>[bBrR][\dxXzZ]* \S*)(?=\s)"
        )

        currpos = simulation_start
        time_stamp = 0
        while True:
            # Get next simulation command from the search space.
            if comment_span:
                # Ignore comment.
                if currpos < comment_span[0]:
                    # Search until comment begins.
                    expr = pat_vcc.search(m, pos=currpos, endpos=comment_span[0])
                    if not expr:
                        # Comment right after enddefinitions-declaration.
                        # ! is this safe?
                        currpos = comment_span[1]
                        continue
                elif comment_span[0] <= currpos and currpos < comment_span[1]:
                    # Jump over the comment.
                    currpos = comment_span[1]
                    continue
                else:
                    # Can safely search until the end.
                    expr = pat_vcc.search(m, pos=currpos)
            else:
                expr = pat_vcc.search(m, pos=currpos)

            if not expr:
                # All simulation commands have been processed.
                break

            # Move the search space limiter forward.
            currpos = expr.span()[1]

            if expr.lastgroup == "timestamp":
                # Set the new time stamp.
                time_stamp = int(expr.group("timestamp").decode("utf8"))

            elif expr.lastgroup == "scalar":
                # Register a scalar value change.
                # Scalar value change (svc) format in BNF:
                #   svc ::= new_value identifier
                # The new value can be 0, 1, x, X, z or Z.
                # The identifier can be multiple characters.
                scalar = expr.group("scalar").decode("utf8")
                if allowed_identifiers:
                    if scalar[1:] not in allowed_identifiers:
                        continue
                # Add the new value to the correct object's time value
                # collection.
                obi[scalar[1:]].tv[time_stamp] = scalar[0]

            elif expr.lastgroup == "vector":
                # Register a vector value change.
                # Vector value change (vvc) format in BNF:
                #   vvc ::= base_letter new_value space identifier
                # Base letter can be b, B, r or R.
                # New value is compressed version of the vector's new value.
                # A space is between the new value and the identifier.
                # Identifier can be multiple characters.
                vector = expr.group("vector").decode("utf8")
                # Remove the space.
                value, identifier = vector.split(" ")
                if allowed_identifiers:
                    if identifier not in allowed_identifiers:
                        continue
                # Remove the base letter.
                value = value[1:]
                # Validate the new value.
                # ? is this needed? should we trust the input file?
                for symbol in value:
                    if symbol != "0":
                        if symbol != "1":
                            if symbol != "z":
                                if symbol != "x":
                                    if symbol != "Z":
                                        if symbol != "X":
                                            raise InvalidValueChangeCommand(
                                                f"Value change command value "
                                                "{value} symbol {symbol} is "
                                                "illegal.")
                # Add the new value to the correct object's time value
                # collection.
                obi[identifier].tv[time_stamp] = \
                    expand_value(value, int(obi[identifier].size))

    return state

# =============================================================================
# Script execution interface.
# =============================================================================


if __name__ == "__main__":

    from vcdutilities import check_user_python_version
    check_user_python_version()

    import argparse
    import pickle

    from pathlib import Path

    cmdparser = argparse.ArgumentParser(
        prog=f"vcdparser",
        usage=None,
        description=f"TODO",
        epilog=f"TODO",
        parents=[],
        formatter_class=argparse.HelpFormatter,
        prefix_chars="-",
        fromfile_prefix_chars=None,
        argument_default=None,
        conflict_handler="error",
        add_help=True,
        allow_abbrev=True,
        exit_on_error=True
    )

    # --------------------
    # Mandatory arguments.
    # --------------------

    cmdparser.add_argument(
        "input",
        type=Path,
        help="Full path to the VCD file."
    )
    cmdparser.add_argument(
        "output",
        type=Path,
        help="Full path to the file where the VCD object is serialized to."
    )

    # ----------------------------
    # Parsing fine tuning options.
    # ----------------------------

    cmdparser.add_argument(
        "-t", "--targets",
        nargs="*",
        help="Signal names with the hierarchy paths to be parsed separated by "
             "spaces."
    )
    cmdparser.add_argument(
        "-tf", "--targets-file",
        type=Path,
        help="Full path to the file which contains target signals."
    )
    cmdparser.add_argument(
        "-st", "--skip-time",
        help="Let the parser skip simulation time. Format is xy where x is 1, "
             "10 or 100 and y is fs, ps, ns, us, ms or s."
    )

    # -----------------------
    # User interface options.
    # -----------------------

    cmdparser.add_argument(
        "-wptc", "--write-progress-to-console",
        action="store_true",
        help="Let the script to report it's progress to the console."
    )
    cmdparser.add_argument(
        "-spb", "--show-progress-bars",
        action="store_true",
        help="Let the script to show it's parsing progress with progress bar."
    )

    # --------------
    # Other options.
    # --------------

    cmdparser.add_argument(
        "-pp", "--profile-performance",
        type=Path,
        help="Full path to the file where a performance report is written to."
    )

    args = vars(cmdparser.parse_args())

    # ------------------------------
    # Mandatory arguments unpacking.
    # ------------------------------

    input_file = args.get("input", None)
    if not isinstance(input_file, Path):
        raise Exception(f"The input must be a file.")
    if not input_file.exists():
        raise Exception(f"The input file must exist.")
    if not input_file.is_file():
        raise Exception(f"The input file must be a file.")
    if input_file.suffix != ".vcd":
        raise Exception(f"The input file extension must be .vcd.")

    output_file = args.get("output", None)
    if not isinstance(output_file, Path):
        raise Exception(f"The output must be a file.")
    if not output_file.exists():
        raise Exception(f"The output file must exist.")
    if not output_file.is_file():
        raise Exception(f"The output file must be a file.")
    if output_file.suffix != ".pickle":
        raise Exception(f"The output file extension must be .pickle.")

    # --------------------------------------
    # Parsing fine tuning options unpacking.
    # --------------------------------------

    targets = args.get("targets", None)
    targets = targets if targets else []

    targets_file = args.get("targets_file", None)
    if targets_file:
        if not isinstance(targets_file, Path):
            raise Exception(f"The targets file must be a file.")
        if not targets_file.exists():
            raise Exception(f"The targets file must exist.")
        if not targets_file.is_file():
            raise Exception(f"The targets file must be a file.")
        if targets_file.suffix != ".txt":
            raise Exception(f"The targets file extension must be .txt.")
        with targets_file.open("r") as file_handle:
            targets.extend(file_handle.readlines())

    targets = validate_targets(targets)

    skipped_time = args.get("skip_time", None)
    if not skipped_time:
        skipped_time = TimeScale(0, TimeScaleUnit.FEMTOSECOND)
    else:
        result = re.search(r"^([0-9]{1,})([fpnum]{0,1}s{1})\Z", skipped_time)
        if not result:
            raise Exception(f"The skipped time has invalid format.")
        value_str, unit_str = result.groups()
        value = int(value_str)
        unit = TimeScaleUnit.from_string(unit_str)
        skipped_time = TimeScale(value, unit)

    # ---------------------------------
    # User interface options unpacking.
    # ---------------------------------

    write_progress_to_console = args.get("write_progress_to_console", False)
    if not isinstance(write_progress_to_console, bool):
        raise Exception(
            f"The write progress to console option must be a boolean.")

    if write_progress_to_console:
        # The user wants to read scipt progress reports.
        logging.basicConfig(level=logging.INFO)
    else:
        # Report only if something goes wrong.
        logging.basicConfig(level=logging.WARNING)

    show_progress_bars = args.get("show_progress_bars", False)
    if not isinstance(show_progress_bars, bool):
        raise Exception(
            f"The show progress bars option must be a boolean.")

    # --------------
    # Other options.
    # --------------

    profile_performance = args.get("profile_performance", None)
    if profile_performance:
        if not isinstance(profile_performance, Path):
            raise Exception(f"The profile performance must be a file.")
        if not profile_performance.exists():
            raise Exception(f"The profile performance file must exist.")
        if not profile_performance.is_file():
            raise Exception(f"The profile performance file must be a file.")
        if profile_performance.suffix != ".txt":
            raise Exception(
                f"The profile performance file extension must be .txt.")

    # ------------------
    # The parsing phase.
    # ------------------

    with input_file.open("r") as input_stream:
        logging.info(f"Parsing the VCD file (this can take a long time).")
        parser_args = (input_stream, targets, show_progress_bars)

        if profile_performance:
            # Parse and profile.
            from cProfile import Profile
            from pstats import Stats, SortKey

            with Profile() as pr:
                parser_state = parse_vcd(*parser_args)

            logging.info(f"Parsing finished.")

            logging.info(f"Writing the performance report.")
            with profile_performance.open("w") as stream:
                stats = Stats(pr, stream=stream)
                stats.strip_dirs()
                stats.sort_stats(SortKey.TIME)
                stats.print_stats()
            logging.info(f"Performance report finished.")

        else:
            # Just parse.
            parser_state = parse_vcd(*parser_args)
            logging.info(f"Parsing finished.")

    logging.info(f"Serializing and saving the VCD object to the output file.")
    with output_file.open("wb") as stream:
        pickle.dump(parser_state, stream)
    logging.info(f"Serializing finished.")

    logging.info(f"Program finished.")
