"""
A bus is a named and ordered group of signals.
"""

# =============================================================================
# Standard library imports
# =============================================================================

from typing import Dict, Tuple, OrderedDict, Optional, Any
from collections import OrderedDict
from dataclasses import dataclass

# =============================================================================
# Local module imports
# =============================================================================

from vcdtiming import Timing
from vcdsignal import Signal
from vcdsignalkind import SignalKind

# =============================================================================
# BusName definition
# =============================================================================


@dataclass
class BusName:
    """
    A name of a bus.

    Attributes # ! FIXME
    ----------
    with_range_description : str
        The bus name with the range description.

    without_range_description : str
        The bus name without the range description.

    range_description : str
        The range description is the index range of the bus signals.
    """

    # To save memory, to get faster attribute accesses and to deny the creation
    # of new attributes dynamically.
    __slots__ = [
        "path",
        "without_range_description",
        "range_description"
    ]

    path: str
    without_range_description: str
    range_description: str

    def with_range_description(self) -> str:
        if self.range_description != "":
            return f"{self.without_range_description}{self.range_description}"
        else:
            return self.without_range_description

    def with_path(self) -> str:
        wrd = self.with_range_description()
        if self.path != "":
            return f"{self.path}.{wrd}"
        else:
            return wrd

    def __repr__(self) -> str:
        return self.with_range_description()

# =============================================================================
# Bus definition
# =============================================================================


class Bus:
    """
    A named and ordered group of signals.

    Attributes
    ----------
    name : BusName
        The name of the bus.

    kind : str
        The type of all the signals which are part of this bus.

    tv : OrderedDict[int, str]
        The time value pairs of the bus tell at which time instants the bus
        value changes to given value. The dictionary key represents a time
        instant and the dictionary value represents a signal value change.

    timing : Timing
        Bus's binary sequence start time instant, clock period and the unit of
        time.

    binarization_failed : bool
        True if the binarization could not be performed.

    size : int
        The amount of signals which are part of the bus.

    range : Tuple[int, int]
        The indices of the bus signals. The first value must be the most
        significant index and the second value must be the least significant
        index.

    signals : Dict[int, Signal]
        The signals which are part of the bus. The dictionary keys are the
        signal indices and the dictionary values are the signals.

    binary_length : int
        The amount of bits in each signal's binary sequences.
    """

    # To save memory, to get faster attribute accesses and to deny the creation
    # of new attributes dynamically.
    __slots__ = [
        "name",
        "kind",
        "tv",
        "timing",
        "binarization_failed",
        "size",
        "range",
        "signals",
        "binary_length"
    ]

    def __init__(
            self,
            name: BusName,
            kind: SignalKind,
            size: int,
            index_range: Tuple[int, int]) -> None:
        self.name: BusName = name
        self.kind: SignalKind = kind
        self.tv: OrderedDict[int, str] = OrderedDict()
        self.timing: Optional[Timing] = None
        self.binarization_failed: bool = False

        self.size: int = size
        self.range: Tuple[int, int] = index_range
        self.signals: Dict[int, Signal] = dict()
        self.binary_length: int = int()

    def __repr__(self) -> str:
        return self.name.with_range_description()

    def to_dictionary(self) -> Dict[str, Optional[str]]:
        data: Dict[str, Any] = {
            "name": self.name.with_range_description(),
            "kind": str(self.kind),
            "size": str(self.size),
            "range": str(self.range),
            "binarization_failed": str(self.binarization_failed),
            "signals": {},
        }

        for signal in self.signals.values():
            name = signal.name.with_bus_description()
            data["signals"][name] = signal.to_dictionary()

        if not self.binarization_failed:
            assert self.timing is not None
            data.update({
                "offset":
                    f"{self.timing.offset} {self.timing.clock_period.unit}",
                "clock_period": str(self.timing.clock_period),
            })

        return data
