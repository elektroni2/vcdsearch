"""
Definitions that are generally usable.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import logging
import pathlib

from typing import Callable, Hashable, Set, List, Union, Iterable
from vcdtimescale import TimeScale

# =============================================================================
# Global variables
# ? should sets be converted to enums?
# =============================================================================

MODULE_NAME = "VCDSearch"
MODULE_VERSION = "1.0"
MODULE_DESCRIPTION = \
"""
Binarize and search binary sequences from a VCD file.
The target VCD file is parsed to a Python data structure.
All or chosen signals and buses are binarized.
All target sequences are searched from the binarized sequences.
The targets are searched from time instants and time ranges.
Time instant search seeks where the target sequence appears and in which time moments.
Time range search seeks where the target sequence appears and in which time periods.
"""
MODULE_EPILOG = "Made with <3 by Roni Hämäläinen"

TIMESTAMP_SCRIPT_START = str()

DECLARATION_KEYWORDS = {
    "$comment",
    "$date",
    "$enddefinitions",
    "$scope",
    "$timescale",
    "$upscope",
    "$var",
    "$version"}

SIMULATION_KEYWORDS = {
    "$dumpall",
    "$dumpoff",
    "$dumpon",
    "$dumpvars"}

VARIABLE_TYPES = {
    "event",
    "integer",
    "parameter",
    "real",
    "reg",
    "supply0",
    "supply1",
    "time",
    "tri",
    "triand",
    "trior",
    "trireg",
    "tri0",
    "tri1",
    "wand",
    "wire",
    "wor"}

BINARY_DIGITS = {"0", "1"}
NON_ZERO_DECIMAL_DIGITS = {"1", "2", "3", "4", "5", "6", "7", "8", "9"}
DECIMAL_DIGITS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
X_DIGITS = {"x", "X"}
Z_DIGITS = {"z", "Z"}
DECIMAL_BASE = {"d", "D"}
SCALAR_DIGITS = BINARY_DIGITS.union(X_DIGITS.union(Z_DIGITS))

TIME_NUMBERS = {"1", "10", "100"}

# ? to enumeration?
SCOPE_TYPES = {
    "begin",
    "fork",
    "function",
    "module",
    "task"}

# =============================================================================
# Exception definitions
# =============================================================================


class PathDoesNotExist(Exception):
    """
    Raised when a path given by the user does not exist.
    """

    def __init__(self, path: pathlib.Path) -> None:
        self.message = f"Given path {path} does not exist."
        super().__init__(self.message)


class PathDoesNotPointToFile(Exception):
    """
    Raised when a path given by the user does not point to a file.
    """

    def __init__(self, path: pathlib.Path) -> None:
        self.message = f"Given path {path} does not point to a file."
        super().__init__(self.message)


class IllegalFileExtension(Exception):
    """
    Raised when a path given by the user does not point to a file with a
    correct file extension.
    """

    def __init__(self, path: pathlib.Path, legal_extensions: Set[str]) -> None:
        legals: Union[str, List[str]] = list(legal_extensions)
        legals = sorted(legals)
        legals = " ".join(legals)
        self.message = f"Given file {path} extension must be one of the \
                         following: {legals}"
        super().__init__(self.message)

# =============================================================================
# Function definitions
# =============================================================================


def remove_duplicates(collection: List[Hashable]) -> List[Hashable]:
    """
    Remove duplicate items from a list while preserving the order of the items.

    Parameters
    ----------
    collection: List[Hashable]
        The target collection with items that can be hashed.

    Returns
    -------
    List[Hashable]
        The target collection without duplicate items.

    Raises
    ------
    Exception
        The target collection was not a list.
    """

    if not isinstance(collection, list):
        raise Exception(f"The collection must be a list.")

    result: List[Hashable] = list()
    identified: Set[Hashable] = set()

    # To avoid re-evaluating the dot expression.
    append = result.append
    add = identified.add

    for item in collection:
        if item not in identified:
            append(item)
            add(item)

    return result


# ? deprecated?
def verify_that_path_points_to_file(path: pathlib.Path) -> None:
    """
    Check that the given path exists and points to a file.

    Parameters
    ----------
    path: pathlib.Path
        The path under verification.

    Raises
    ------
    PathDoesNotExist
        Given path does not point to a existing file or directory.
    PathDoesNotPointToFile
        Given path does not point to a file.
    """

    if not isinstance(path, pathlib.Path):
        raise PathDoesNotExist(path)

    if not path.exists():
        raise PathDoesNotExist(path)

    if not path.is_file():
        raise PathDoesNotPointToFile(path)


# ? deprecated?
def verify_that_path_points_to_file_with_extension(
        path: pathlib.Path,
        legal_extensions: Set[str]) -> None:
    """
    Check that the given path points to a existing file with a correct file
    extension.

    Parameters
    ----------
    path: pathlib.Path
        The path under verification.
    legal_extensions: Set[str]
        The set of all file extensions that are considered legal.

    Raises
    ------
    PathDoesNotExist
        Given path does not point to a existing file or directory.
    PathDoesNotPointToFile
        Given path does not point to a file.
    IllegalFileExtension
        Given path does not have a allowed extension.
    """

    verify_that_path_points_to_file(path)

    if path.suffix not in legal_extensions:
        raise IllegalFileExtension(path, legal_extensions)


# -----------------------------------------------------------------------------
# User interface functions
# -----------------------------------------------------------------------------


def check_user_python_version() -> None:
    """
    Check that the user uses the minimum required Python version.
    """
    from sys import version_info as vi
    if vi < (3, 9, 1):
        version = f"{vi.major}.{vi.minor}.{vi.micro}"
        logging.warning(f"This script has only been tested with Python \
                          version 3.9.1 while your version is {version}.")


# ? deprecated?
def text_lined(text: str) -> str:
    """
    Add over- and underline to a text.

    Parameters
    ----------
    text: str
        The piece of text where the over- and underlines are added to.

    Returns
    -------
    str
        The text with over- and underline.
    """

    text_lines = text.splitlines()
    length = max(len(text_line) for text_line in text_lines)

    line = "".join(["="] * length)
    return f"{line}\n{text}\n{line}"


class WrapperWithNoProgressBar:
    def __init__(self, iterable: Iterable, **kwargs) -> None:
        self.iterable = iterable

    def __enter__(self) -> Iterable:
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        return

    def __iter__(self):
        return iter(self.iterable)

    def update(self, number: int) -> None:
        return


def wrapper_with_no_progress_bar(iterable: Iterable, **kwargs) -> Iterable:
    """
    If the user did not want to use the progress bars or the module that
    implements them was not found, then this function is used to wrap the
    iterable as a mock tqdm wrapper.

    Parameters
    ----------
    iterable: Iterable
        The collection which would have been iterated using the progress bar
        wrapper.

    Returns
    -------
    Iterable
        Just the same iterable without any modification.
    """

    return iterable


def get_progress_bar_wrapper(show_progress_bars: bool) -> Callable:
    """
    Select the progress bar wrapper for the sequence iterator.

    Parameters
    ----------
    show_progress_bars: bool
        TODO

    Returns
    -------
    Callable
        TODO
    """

    if show_progress_bars:
        try:
            from tqdm import tqdm
            return tqdm
        except ImportError:
            logging.warning(f"Could not import the tqdm module which"
                            "implements the progress bars.")

    #return wrapper_with_no_progress_bar
    return WrapperWithNoProgressBar


def validate_and_extract_time_expression(expression: str) -> TimeScale:
    """
    TODO
    """

    # Attempt to extract the value and the unit.
    results = re.search(r"^([0-9]{1,})([fpnum]{0,1}s{1})\Z", expression)
    if not results:
        raise InvalidTimeExpression()

    value_str, unit_str = results.groups()

    value = int(value_str)
    unit = TimeScaleUnit.from_string(unit_str)

    return TimeScale(value, unit)
