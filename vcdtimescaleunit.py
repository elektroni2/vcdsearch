"""
A time scale unit is a definite magnitude of a time expression. In other words,
it scales an expression of time.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import enum

from typing import Set, List

# =============================================================================
# Exception definitions
# =============================================================================


class UnknownTimeScaleUnit(Exception):
    """
    Raised when the textual representation of a time scale unit is unknown.
    """

    def __init__(self, string: str) -> None:
        self.message = f"Unknown time scale unit: {string}"
        super().__init__(self.message)


# =============================================================================
# TimeScaleUnit definition
# =============================================================================


class TimeScaleUnit(enum.Enum):
    """
    An enumeration of different units that scale a time expression.
    """

    FEMTOSECOND = 0
    PICOSECOND = 1
    NANOSECOND = 2
    MICROSECOND = 3
    MILLISECOND = 4
    SECOND = 5

    @staticmethod
    def units() -> Set[str]:
        """
        A set of the textual symbols which represent magnitudes of the scales
        of a time expression.
        """
        return {"fs", "ps", "ns", "us", "ms", "s"}

    @staticmethod
    def units_ordered() -> List[str]:
        """
        A list of the textual symbols which represent magnitudes of the scales
        of a time expression ordered from the smallest to the largest.
        """
        return ["fs", "ps", "ns", "us", "ms", "s"]

    @staticmethod
    def from_string(string: str) -> "TimeScaleUnit":
        """
        Transform a textual time scale unit representation to a enumeration.

        Parameters
        ----------
        string: str
            The time scale unit represented as a text.

        Returns
        -------
        TimeScaleUnit
            The time scale unit transformed from text to a enumeration object.

        Raises
        ------
        UnknownTimeScale
            Given time scale unit is unknown.
        """

        if string == "fs":
            return TimeScaleUnit.FEMTOSECOND
        elif string == "ps":
            return TimeScaleUnit.PICOSECOND
        elif string == "ns":
            return TimeScaleUnit.NANOSECOND
        elif string == "us":
            return TimeScaleUnit.MICROSECOND
        elif string == "ms":
            return TimeScaleUnit.MILLISECOND
        elif string == "s":
            return TimeScaleUnit.SECOND
        else:
            raise UnknownTimeScaleUnit(string)

    def to_scalar(self) -> float:
        """
        Transform a time scale unit enumeration object to a scalar number.

        Returns
        -------
        float
            The time scale unit as a floating point multiplier.
        """

        if self is TimeScaleUnit.FEMTOSECOND:
            return 0.000000000000001
        elif self is TimeScaleUnit.PICOSECOND:
            return 0.000000000001
        elif self is TimeScaleUnit.NANOSECOND:
            return 0.000000001
        elif self is TimeScaleUnit.MICROSECOND:
            return 0.000001
        elif self is TimeScaleUnit.MILLISECOND:
            return 0.001
        elif self is TimeScaleUnit.SECOND:
            return 1.0

    def __str__(self) -> str:
        if self is TimeScaleUnit.FEMTOSECOND:
            return "fs"
        elif self is TimeScaleUnit.PICOSECOND:
            return "ps"
        elif self is TimeScaleUnit.NANOSECOND:
            return "ns"
        elif self is TimeScaleUnit.MICROSECOND:
            return "us"
        elif self is TimeScaleUnit.MILLISECOND:
            return "ms"
        elif self is TimeScaleUnit.SECOND:
            return "s"

    def __repr__(self) -> str:
        return self.__str__()

    def __lt__(self, other) -> bool:
        if not isinstance(other, TimeScaleUnit):
            raise Exception(f"You must only compare time scale units to other"
                            "time scale units.")
        return self.to_scalar() < other.to_scalar()
