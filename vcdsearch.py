"""
# ! TODO: this module might be deprecated?
# ! TODO: create how-to-use-documentation
# ? custom exceptions with 1. what went wrong 2. how to fix it
# ? checks with assertions, meta class subclass requirements, decorators
# ? graphlib for signal hierarchy manipulation
# ? compress binaries with binascii -> reduce memory usage
# ? gprof2dot, profiling results to PNG image

VCDSearch 1.0

This tool enables you to:
(1) create a Python data structure from a VCD (value change dump) file,
(2) search and report the appereances of a interesting binary sequence.

In the future this tool might enable you to:
(1) trace the movement of the sequence of interest,
(2) compare at least two VCD files.

Commonly used concepts:
(1) Time value pair: a pair of a time instant and a signal value.
(2) Signal: a container of time value pairs with a name.
(3) Bus: a signal with multiple signal values for a single time instant.
"""

# =============================================================================
# Standard library imports
# =============================================================================

import logging
import datetime

# =============================================================================
# Local module imports
# =============================================================================

from vcdutilities import \
    IllegalFileExtension, \
    PathDoesNotPointToFile, \
    TIMESTAMP_SCRIPT_START, \
    PathDoesNotExist

from vcdparser import ParserState

from vcdinputvalidator import \
    CommandLineArguments, \
    EmptyTargetList, \
    TargetWasNotASCII, \
    NoBracketsInDoubleColonNotationSignal, \
    BusDescriptionIndexWasNotNumber, \
    LeftBusDescriptionIndexNotGreater

# =============================================================================
# Function definitions
# =============================================================================

# -----------------------------------------------------------------------------
# Processing setup functions.
# -----------------------------------------------------------------------------


def read_command_line_arguments() -> CommandLineArguments:
    """
    TODO
    """

    from vcdinputvalidator import parse_command_line_arguments

    # ! TODO
    # ! add other exceptions
    try:
        args = parse_command_line_arguments()
    except PathDoesNotExist:
        logging.exception(
            f"You must give a path that exists.")
        quit()
    except PathDoesNotPointToFile:
        logging.exception(
            f"You must give a path that points to a existing file.")
        quit()
    except IllegalFileExtension:
        logging.exception(
            f"You must give a path to a file with a legal file extension.")
        quit()
    except TargetWasNotASCII:
        logging.exception(
            f"You must use ASCII encoding when expressing the target signals "
            "and sequences.")
        quit()
    except EmptyTargetList:
        logging.exception(
            f"You must give at least one target when using commands "
            "--target-signals and --target-sequences.")
        quit()
    except OSError:
        logging.exception(
            f"You must give a path to a file that can be opened.")
        quit()
    except NoBracketsInDoubleColonNotationSignal:
        logging.exception(
            f"You must use the bus brackets with the double colon notation. "
            "For example: targets[3::0]")
        quit()
    except BusDescriptionIndexWasNotNumber:
        logging.exception(
            f"You must use numbers as indices inside the bus brackets. For "
            "example: targets[3:0]")
        quit()
    except LeftBusDescriptionIndexNotGreater:
        logging.exception(
            f"You must put the most significant bus index to left and the "
            "least significant bus index to right inside the bus brackets. "
            "For example: targets[3:0]")
        quit()
    except Exception:
        logging.exception(
            f"Could not parse the command line arguments.")
        quit()

    if args.write_progress_to_console:
        # The user wants to read the script progress reports.
        logging.basicConfig(level=logging.INFO)
    else:
        # Report only if something goes wrong.
        logging.basicConfig(level=logging.WARNING)

    return args

# -----------------------------------------------------------------------------
# Processing functions.
# -----------------------------------------------------------------------------


def process_vcd_file(args: CommandLineArguments) -> ParserState:
    """
    Obtain the VCD object by parsing a VCD file or deserializing a serialized
    VCD object file.

    Parameters
    ----------
    args: CommandLineArguments
        The arguments fed by the user.

    Returns
    -------
    ParserState
        The state of the parser after the parsing. This also contains the VCD
        object.

    Raises
    ------
    TODO
    """

    parse_vcd_file_to_python_object = args.target_file is not None

    if parse_vcd_file_to_python_object:
        from vcdreporting import report_parsing_results

        logging.info(f"Parsing the VCD file (this can take a long time).")

        from vcdparser import parse_vcd

        try:
            parser_state = parse_vcd(args.parser_arguments)

        except Exception:
            logging.exception(
                f"Could not parse the VCD file to a Python object.")
            quit()

        if parser_state:
            report_parsing_results(
                parser_state,
                args.write_parsing_report_to_console,
                args.parsing_report_files)

            if args.serialized_vcd_object_file:
                # The user wants to save the VCD object to a file.

                logging.info(
                    f"Serializing and saving the VCD object to a picle-file.")

                import pickle

                with args.serialized_vcd_object_file.open("wb") as file_handle:
                    pickle.dump(parser_state, file_handle)

        # ! variable signal binary lenght is calculated to be 1112 and clock period 1
        # ! vcdtestfile has picoseconds NOTICE this

    else:
        # The user wants to load the VCD object from a file.

        logging.info(
            f"Deserializing and loading the VCD object from a pickle-file.")

        import pickle

        with args.target_object_file.open("rb") as file_handle:
            parser_state = pickle.load(file_handle)

    return parser_state


def process_vcd_object(parser_state: ParserState, args: CommandLineArguments):
    """
    TODO
    """

    if not parser_state:
        logging.info(f"No parser state given. Binarization and search are not "
                     "performed.")
        return

    from vcdbinarizer import execute_binarization
    from vcdsearcher import execute_search
    from vcdreporting import \
        report_binarization_results, \
        report_search_results

    logging.info(f"Transforming the signals to binary sequences.")

    vcd = parser_state.vcd_object

    try:
        execute_binarization(vcd, args.show_progress_bars)

        report_binarization_results(
            vcd,
            args.write_binaries_to_console,
            args.binaries_files)

    except Exception:
        # ? is this a fatal error or should the parsed object be saved?
        logging.exception(f"Could not binarize the signals and buses.")
        quit()

    if not args.target_sequences:
        logging.info(f"No target sequences selected. Search is not performed.")
        return

    logging.info(f"Searching for target binary sequences.")

    try:
        search_results = execute_search(
            vcd,
            args.target_sequences,
            args.show_progress_bars)

    except Exception:
        # ! handle this better
        # ? is this a fatal error or should the parsed object be saved?
        logging.exception(
            f"Could not perform the search from the signals and buses.")
        quit()

    try:
        report_search_results(
            search_results,
            args.write_search_report_to_console,
            args.search_report_files)

    except Exception:
        # ! handle this better
        logging.exception(f"Could not report the search results.")

# -----------------------------------------------------------------------------
# Main function definition
# -----------------------------------------------------------------------------


def main() -> None:

    # Check that the user uses the minimum required Python version.
    from vcdutilities import check_user_python_version
    check_user_python_version()

    # ? is needed?
    global TIMESTAMP_SCRIPT_START
    TIMESTAMP_SCRIPT_START = datetime.datetime.today().isoformat()

    args = read_command_line_arguments()

    from vcdutilities import MODULE_NAME, MODULE_VERSION
    logging.info(f"{MODULE_NAME} {MODULE_VERSION}")

    if args.performance_report_files:
        # Parse, process and profile the performance.
        from cProfile import Profile
        from pstats import Stats, SortKey

        with Profile() as pr:
            parser_state = process_vcd_file(args)
            process_vcd_object(parser_state, args)

        for path in args.performance_report_files:
            if path.suffix == ".txt":
                with path.open("w") as stream:
                    stats = Stats(pr, stream=stream)
                    stats.strip_dirs()
                    stats.sort_stats(SortKey.TIME)
                    stats.print_stats()
            elif path.suffix == ".json":
                raise NotImplementedError()
            elif path.suffix == ".xml":
                raise NotImplementedError()
            elif path.suffix == ".csv":
                raise NotImplementedError()

    else:
        # Just parse and process.
        parser_state = process_vcd_file(args)
        process_vcd_object(parser_state, args)

    logging.info(f"Exiting from {MODULE_NAME} {MODULE_VERSION}")

# =============================================================================
# Script execution interface.
# =============================================================================


if __name__ == "__main__":
    main()
