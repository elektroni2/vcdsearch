"""
A signal kind alias signal type tells what a signal represents or is part of.
"""

# =============================================================================
# Standard library imports
# =============================================================================

from enum import Enum
from typing import Set

# =============================================================================
# Exception definitions
# =============================================================================


class UnknownSignalKind(Exception):
    """
    Raised when the signal kind is unknown.
    """

    def __init__(self, string: str) -> None:
        self.message = f"Unknown signal type: {string}"
        super().__init__(self.message)


# =============================================================================
# SignalKind definition
# =============================================================================


class SignalKind(Enum):
    """
    A type of a signal.

    A signal type belongs either to a net or a register. A net must be
    constantly driven. A register retains the last value assigned to it.
    """

    # -------------------------------
    # Net data types (synthesizable).
    # -------------------------------
    # Wire and tri are equivalent. Wire should be used for nets with one driver
    # and tri should be used for nets with multiple drivers.
    WIRE = "wire"
    TRI = "tri"

    # Same as wire/tri, but if a same net is driven by multiple drivers, the
    # xand adds an AND-gate and the xor adds an OR-gate between the drivers and
    # the net identified with the reference.
    WOR = "wor"
    TRIOR = "trior"
    WAND = "wand"
    TRIAND = "triand"

    # Power supply model that drives a constant 0 or 1 state to the net.
    SUPPLY0 = "supply0"
    SUPPLY1 = "supply1"

    # -----------------------------------
    # Net data types (not synthesizable).
    # -----------------------------------

    # When the net has no drivers or all the drivers are driving Z, the net
    # will be pulled to 0 or 1 state. Models for pull-up/pulldown resistors.
    TRI0 = "tri0"
    TRI1 = "tri1"

    # When all net drivers are pulled Z, trireg stores the net value and drives
    # the net with that value. Models capacitive storage.
    TRIREG = "trireg"

    # ------------------------------------
    # Register data types (synthesizable).
    # ------------------------------------

    REG = "reg"
    INTEGER = "integer"
    PARAMETER = "parameter"

    # ----------------------------------------
    # Register data types (not synthesizable).
    # ----------------------------------------

    EVENT = "event"
    REAL = "real"
    TIME = "time"

    # -------------------------------------------------------------------------
    # Interface definitions.
    # -------------------------------------------------------------------------

    def __str__(self) -> str:
        return self.value

    def __repr__(self) -> str:
        return self.value

    def all_types(self) -> Set[str]:
        return {
            "event",
            "integer",
            "parameter",
            "real",
            "reg",
            "supply0",
            "supply1",
            "time",
            "tri",
            "triand",
            "trior",
            "trireg",
            "tri0",
            "tri1"
            "wand",
            "wire",
            "wor"
        }
